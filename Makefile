-include Makefile.rules

.PHONY:plugins

all:project plugins

project:
	cd src/ui && $(MAKE) all

plugins:
	cd src/plugins && $(MAKE) all

Makefile.rules: configure
	./configure

clean:
	find . -name '*.o' -exec rm -vf {} \;
	find . -name 'Makefile.rules' -exec rm -vf {} \;
	find . -name '*.deps' -exec rm -vf {} \;
	find . -name 'core' -exec rm -vf {} \;
	find . -name '*.log' -exec rm -vf {} \;
	find . -name '*~' -exec rm -vf {} \;
	find . -name '*#*' -exec rm -vf {} \;

cleanplugins:
	find . -name '*.so' -exec rm -vf {} \;

distclean: clean cleanplugins
	rm -vf $(PROJECT)
	find . -name '*.a' -exec rm -vf {} \;
	find . -name '.svn*' -exec rm -vf {} \;
	find . -name '*.tgz' -exec rm -vf {} \;

dist: distclean
	cd ../ && tar -cvf $(PROJECT).tgz $(PROJECT)
	@echo -n ".------"
	@echo -n `echo "Tarball is ready:" $(PROJECT).tgz | sed s/./-/g`
	@echo "."
	@echo -n "|   "
	@echo -n "Tarball is ready:" $(PROJECT).tgz
	@echo "   |"
	@echo -n "'------"
	@echo -n `echo "Tarball is ready:" $(PROJECT).tgz | sed s/./-/g`
	@echo "'"
	@$(HASH) ../$(PROJECT).tgz
