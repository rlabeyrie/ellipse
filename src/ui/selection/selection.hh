#ifndef SELECTION_HH_
# define SELECTION_HH_

# include <QImage>

class Selection
{
public:
  Selection (QImage&);
  Selection (Selection&);
  Selection (unsigned, unsigned);
  Selection (Selection&, float);
  Selection (QImage&, float);
  unsigned* operator[] (unsigned) const;
  unsigned height () const;
  unsigned width () const;
  unsigned x () const;
  unsigned y () const;
private:
  unsigned** map_;
  unsigned w_;
  unsigned h_;
  unsigned x_;
  unsigned y_;
};

# include "selection.hxx"

#endif /* !SELECTION_HH_ */
