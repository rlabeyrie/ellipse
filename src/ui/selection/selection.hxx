#ifndef SELECTION_HXX_
# define SELECTION_HXX_

# include <iostream>

inline
Selection::Selection (QImage& img)
  : w_ (img.width ()),
    h_ (img.height ())
{
  map_ = new unsigned* [h_];
  for (unsigned i = 0; i < h_; ++i)
  {
    map_[i] = new unsigned [w_];
    for (unsigned j = 0; j < w_; ++j)
      map_[i][j] = img.pixel (j, i);
  }
}

inline
Selection::Selection (QImage& img, float ratio)
  : w_ (img.width () * ratio),
    h_ (img.height () * ratio)
{
  float fact = 1 / ratio;
  map_ = new unsigned* [h_];
  for (unsigned i = 0; i < h_; ++i)
  {
    map_[i] = new unsigned [w_];
    for (unsigned j = 0; j < w_; ++j)
      map_[i][j] = img.pixel (j * fact, i * fact);
  }
}

inline
Selection::Selection (Selection& rhs)
  : w_ (rhs.w_),
    h_ (rhs.h_)
{
  map_ = new unsigned* [h_];
  for (unsigned i = 0; i < h_; ++i)
  {
    map_[i] = new unsigned [w_];
    for (unsigned j = 0; j < w_; ++j)
      map_[i][j] = rhs.map_[i][j];
  }
}

inline
Selection::Selection (unsigned w, unsigned h)
  : w_ (w),
    h_ (h)
{
  map_ = new unsigned* [h];
  for (unsigned i = 0; i < h; ++i)
  {
    map_[i] = new unsigned [w];
    for (unsigned j = 0; j < w; ++j)
      map_[i][j] = 0;
  }
}

inline
unsigned* Selection::operator[] (unsigned i) const
{
  return map_[i];
}

inline
unsigned Selection::height () const
{
  return h_;
}

inline
unsigned Selection::width () const
{
  return w_;
}

#endif /* !SELECTION_HXX_ */
