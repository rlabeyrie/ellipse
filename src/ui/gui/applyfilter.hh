#ifndef APPLYFILTER_HH_
# define APPLYFILTER_HH_

# include <QUndoCommand>
# include <QThread>

// forward decs
class Plugin;
class Selection;
class FilterThread;

class ApplyFilter : public QThread,
		    public QUndoCommand
{
  Q_OBJECT;
public:
  ApplyFilter (Plugin* plugin, Selection* selection);
  ~ApplyFilter ();
  virtual void undo ();
  virtual void redo ();
  virtual void run ();
public slots:
  void refreshProgress ();
private:
  Plugin* plugin_;
  Selection* undoSelect_;
  Selection* redoSelect_;
};

#endif /* !APPLYFILTER_HH_ */
