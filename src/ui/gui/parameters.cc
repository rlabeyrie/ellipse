#include "parameters.hh"
#include "parameters.moc"
#include "plugin.hh"
#include "filters.hh"
#include "parametervisitor.hh"
#include "gradientwidget.hh"

#include <QWidget>
#include <QGridLayout>
#include <QLabel>
#include <QPushButton>
#include <QScrollArea>
#include <QColor>
#include <QSize>
#include <iostream>

Parameters::Parameters (QWidget* parent)
  : QDockWidget (parent),
    content_ (0)
{
  setWindowTitle ("Parameters");
  setObjectName ("Parameters");
  setAllowedAreas (Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
  setMinimumSize (100, 100);
}

Parameters::~Parameters ()
{
}

void Parameters::reloadParameters ()
{
  if (content_)
    delete content_;
  content_ = new QWidget (this);
  setWidget (content_);
  QGridLayout* layout = new QGridLayout (content_);
  content_->setLayout (layout);
  QScrollArea* area = new QScrollArea (content_);
  area->setWidgetResizable (true);
  area->setFrameStyle (QFrame::NoFrame);
  QGridLayout* areaLayout = new QGridLayout (area);
  QWidget* areaContent = new QWidget (area);
  areaContent->setLayout (areaLayout);
  area->setWidget (areaContent);
  QLabel* label = new QLabel (area);
  label->setWordWrap (true);
  label->setAlignment (Qt::AlignTop | Qt::AlignRight);
  label->setText (FILTERS->current ()->name ());
  layout->addWidget (area, 0, 0);
  QPushButton* button = new QPushButton (content_);
  button->setText ("Apply");
  button->setStatusTip ("Apply the selected filter.");
  layout->addWidget (button, 1, 0);
  layout->setColumnStretch (0, 1);
  layout->setRowStretch (1, 1);
  layout->setMargin (0);
  layout->setRowMinimumHeight (0, 130);

  ParameterVisitor visitor (areaContent, areaLayout);
  std::deque<Parameter*>& l = FILTERS->current ()->params ();
  for (unsigned i = 0; i < l.size (); ++i)
    l[i]->accept (visitor);
  areaLayout->addWidget (label, 0, 0, 1, 10);
  areaLayout->setMargin (0);
  areaLayout->setColumnMinimumWidth (0, 15);
  areaLayout->setColumnStretch (0, 0);
  areaLayout->setRowStretch (0, 0);

  visitor.lastRow ();
  QObject::connect (button, SIGNAL (released ()),
		    FILTERS, SLOT (process ()));
}
