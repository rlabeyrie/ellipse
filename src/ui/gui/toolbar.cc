#include "toolbar.hh"
#include "toolbar.moc"
#include "tabs.hh"
#include "history.hh"

#include <QWidget>
#include <QAction>
#include <QObject>
#include <QUndoGroup>

ToolBar::ToolBar (QWidget* parent)
  : QToolBar (parent)
{
  setWindowTitle ("Toolbar");
  setObjectName ("Toolbar");
  QAction* open = addAction ("Open");
  addAction (HISTORY->group ()->createUndoAction (this));
  addAction (HISTORY->group ()->createRedoAction (this));
  QObject::connect (open, SIGNAL(triggered()), TABS, SLOT(open()));
}

ToolBar::~ToolBar ()
{
}
