#ifndef HISTORY_HH_
# define HISTORY_HH_

# include <QDockWidget>
# include <map>
# include "singleton.hh"

# define HISTORY Singleton<History>::instance()

// forward decs
class QWidget;
class QUndoGroup;
class QUndoView;
class QUndoCommand;
class QUndoStack;

class History : public QDockWidget
{
  Q_OBJECT;
public:
  ~History ();
  History (QWidget* parent = 0);
  void push (QUndoCommand*);
  QUndoGroup* group () const;
  void newStack (int);
public slots:
  void changeActiveStack (int);
  void cancel ();
private:
  QUndoView* view_;
  QUndoGroup* group_;
  std::map<int, QUndoStack*> stacks_;
};

#endif /* !HISTORY_HH_ */
