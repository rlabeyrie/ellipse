#include "filters.hh"
#include "filters.moc"

#include "plugin.hh"
#include "selection.hh"
#include "tabs.hh"
#include "pictureview.hh"
#include "applyfilter.hh"
#include "history.hh"
#include "statusbar.hh"
#include "parameters.hh"
#include "toolbar.hh"
#include "preview.hh"

#include <QWidget>
#include <QTreeWidget>
#include <QTreeWidgetItem>
#include <QString>
#include <iostream>
#include <QScrollArea>
#include <QGridLayout>
#include <QLabel>
#include <QImage>

Filters::Filters (QWidget* parent)
  : QDockWidget (parent),
    activeFilter_ (0),
    tempselect_ (0)
{
  setWindowTitle ("Filters");
  setObjectName ("Filters");
  setAllowedAreas (Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
  setMinimumSize (100, 100);
  QWidget* content = new QWidget (this);
  setWidget (content);
  QGridLayout* layout = new QGridLayout (content);
  content->setLayout (layout);
  layout->setMargin (0);
  tree_ = new QTreeWidget (content);
  tree_->setHeaderHidden (true);
  tree_->setStatusTip ("Select the filter you want to apply.");
  layout->addWidget (tree_, 0, 0);
  QScrollArea* area = new QScrollArea (content);
  layout->addWidget (area, 1, 0);
  layout->setRowStretch (0, 1);
  layout->setRowMinimumHeight (1, 40);
  desc_ = new QLabel (area);
  area->setWidget (desc_);
  area->setWidgetResizable (true);
  area->setFrameStyle (QFrame::NoFrame);
  desc_->setWordWrap (true);
  QObject::connect (tree_, SIGNAL (currentItemChanged (QTreeWidgetItem*,
						       QTreeWidgetItem*)),
		    this, SLOT (emitFilterChanged (QTreeWidgetItem*,
						   QTreeWidgetItem*)));
}

Filters::~Filters ()
{
}

void Filters::newEntry (Plugin* plugin)
{
  QTreeWidgetItem* item = new QTreeWidgetItem (0);
  item->setText (0, plugin->name ());
  if (!levels_.size ())
    tree_->addTopLevelItem (item);
  else
    levels_.back ()->addChild (item);
  table_[item] = plugin;
  if (activeFilter_ == 0)
    activeFilter_ = plugin;
}

void Filters::treeLevelUp (QString name)
{
  QTreeWidgetItem* item = new QTreeWidgetItem (0);
  item->setText (0, name);
  if (!levels_.size ())
    tree_->addTopLevelItem (item);
  else
    levels_.back ()->addChild (item);
  levels_.push_back (item);
}

void Filters::treeLevelDown ()
{
  levels_.pop_back ();
}

void Filters::emitFilterChanged (QTreeWidgetItem* item, QTreeWidgetItem*)
{
  if (table_.find (item) != table_.end ())
  {
    activeFilter_ = table_[item];
    desc_->setText (activeFilter_->description ());
    emit (filterChanged ());
  }
}

Plugin* Filters::current () const
{
  return activeFilter_;
}

void Filters::process ()
{
  QWidget* current = TABS->currentWidget ();
  if (current)
  {
    QScrollArea* area = (QScrollArea*) (current);
    PictureView* view = (PictureView*) area->widget ();
    Selection* save = new Selection (view->image ());
    apply_ = new ApplyFilter (activeFilter_, save);
    HISTORY->push (apply_);
  }
}

void Filters::process (const Selection* prev, Selection* res)
{
  activeFilter_->process (*prev, *res);
}

void Filters::patch (Selection* select)
{
  QWidget* current = TABS->currentWidget ();
  if (current)
  {
    QScrollArea* area = (QScrollArea*) (current);
    PictureView* view = (PictureView*) area->widget ();
    for (unsigned i = 0; i < select->height (); ++i)
      for (unsigned j = 0; j < select->width (); ++j)
	view->image ().setPixel (j, i, (*select)[i][j]);
    view->repaint ();
  }
}

void Filters::anticipatedPatch (Selection* s)
{
  tempselect_ = s;
}

void Filters::flushPatch ()
{
  patch (tempselect_);
  TABS->setEnabled (true);
  PARAMS->setEnabled (true);
  TOOLBAR->setEnabled (true);
  HISTORY->setEnabled (true);
  setEnabled (true);
}

void Filters::cancel ()
{
  delete apply_;
  STATUS->endTimer ();
  TABS->setEnabled (true);
  PARAMS->setEnabled (true);
  TOOLBAR->setEnabled (true);
  HISTORY->setEnabled (true);
  setEnabled (true);
}
