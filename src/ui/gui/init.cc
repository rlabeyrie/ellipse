#include "init.hh"

template <>
Filters* Singleton<Filters>::instance_ = 0;
template <>
Layers* Singleton<Layers>::instance_ = 0;
template <>
Parameters* Singleton<Parameters>::instance_ = 0;
template <>
MainWindow* Singleton<MainWindow>::instance_ = 0;
template <>
MenuBar* Singleton<MenuBar>::instance_ = 0;
template <>
StatusBar* Singleton<StatusBar>::instance_ = 0;
template <>
ToolBar* Singleton<ToolBar>::instance_ = 0;
template <>
Tabs* Singleton<Tabs>::instance_ = 0;
template <>
Loader* Singleton<Loader>::instance_ = 0;
template <>
History* Singleton<History>::instance_ = 0;
template <>
Preview* Singleton<Preview>::instance_ = 0;
template <>
Tools* Singleton<Tools>::instance_ = 0;
