#include "filterthread.hh"
#include "filterthread.moc"

#include "filters.hh"
#include "plugin.hh"

FilterThread::FilterThread (Plugin* plugin,
			    Selection* select,
			    QObject* parent)
  : QThread (parent),
    plugin_ (plugin),
    select_ (select)
{
}

void FilterThread::run ()
{
  plugin_->process (*select_);
  FILTERS->patch (select_);
}
