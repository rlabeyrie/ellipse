#ifndef PARAMETERVISITOR_HH_
# define PARAMETERVISITOR_HH_

# include "parameter.hh"

// forward decs
class QGridLayout;
class QWidget;

class ParameterVisitor
{
public:
  ParameterVisitor (QWidget* parent = 0,
		    QGridLayout* layout = 0);
  void lastRow ();
  void visit (const CheckParameter*);
  void visit (const RadioParameter*);
  void visit (const SliderParameter*);
  void visit (const ColorParameter*);
  void visit (const GradientParameter*);
private:
  void showDesc (const Parameter*);
private:
  QWidget* parent_;
  QGridLayout* layout_;
  unsigned row_;
};

#endif /* !PARAMETERVISITOR_HH_ */
