#ifndef STATUSBAR_HH_
# define STATUSBAR_HH_

# include <QStatusBar>
# include "singleton.hh"

# define STATUS Singleton<StatusBar>::instance()

// forward decs
class QWidget;
class QProgressBar;
class QPushButton;
class QTimer;

class StatusBar : public QStatusBar
{
  Q_OBJECT;
public:
  StatusBar (QWidget* parent = 0);
  ~StatusBar ();
  QPushButton* cancel () const;
  QTimer* timer () const;
  QProgressBar* pbar () const;
  void endTimer ();
  void newTimer ();
  void refreshProgress (int);
public slots:
  void showProgress ();
  void hideProgress ();
private:
  QProgressBar* pbar_;
  QPushButton* cancel_;
  QTimer* timer_;
};

#endif /* !STATUSBAR_HH_ */
