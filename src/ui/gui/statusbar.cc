#include "statusbar.hh"
#include "statusbar.moc"

#include <QWidget>
#include <QProgressBar>
#include <QPushButton>
#include <QTimer>

StatusBar::StatusBar (QWidget* parent)
  : QStatusBar (parent)
{
  pbar_ = new QProgressBar (this);
  pbar_->setMaximumWidth (150);
  pbar_->setValue (0);
  cancel_ = new QPushButton (this);
  cancel_->setText ("Cancel");
  cancel_->setStatusTip ("Cancels the execution of the curent filter.");
  addPermanentWidget (cancel_);
  addPermanentWidget (pbar_);
  cancel_->setVisible (false);
  pbar_->setVisible (false);
  timer_ = new QTimer (this);
}

StatusBar::~StatusBar ()
{
}

void StatusBar::showProgress ()
{
  pbar_->setValue (0);
  pbar_->setVisible (true);
  cancel_->setVisible (true);
}

void StatusBar::hideProgress ()
{
  pbar_->setVisible (false);
  cancel_->setVisible (false);
}

QPushButton* StatusBar::cancel () const
{
  return cancel_;
}

void StatusBar::newTimer ()
{
  timer_ = new QTimer (this);
}

QTimer* StatusBar::timer () const
{
  return timer_;
}

QProgressBar* StatusBar::pbar () const
{
  return pbar_;
}

void StatusBar::endTimer ()
{
  timer_->stop ();
  delete timer_;
}

void StatusBar::refreshProgress (int val)
{
  pbar_->setValue (val);
}
