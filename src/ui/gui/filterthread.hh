#ifndef FILTERTHREAD_HH_
# define FILTERTHREAD_HH_

# include <QThread>

// forward decs
class QObject;
class Plugin;
class Selection;
class FilterThread;

class FilterThread : public QThread
{
  Q_OBJECT;
public:
  FilterThread (Plugin* plugin, Selection* select, QObject* parent = 0);
  virtual void run ();
private:
  Plugin* plugin_;
  Selection* select_;
};

#endif /* !FILTERTHREAD_HH_ */
