#include "tools.hh"
#include "tools.moc"

#include <QWidget>

Tools::Tools (QWidget* parent)
  : QDockWidget (parent)
{
  setWindowTitle ("Tools");
  setObjectName ("Tools");
  setAllowedAreas (Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
  setMinimumSize (100, 100);
}

Tools::~Tools ()
{
}
