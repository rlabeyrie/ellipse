#ifndef TOOLBAR_HH_
# define TOOLBAR_HH_

# include <QToolBar>
# include "singleton.hh"

# define TOOLBAR Singleton<ToolBar>::instance()

// forward decs
class QWidget;

class ToolBar : public QToolBar
{
  Q_OBJECT;
public:
  ToolBar (QWidget* parent = 0);
  ~ToolBar ();
};

#endif /* !TOOLBAR_HH_ */
