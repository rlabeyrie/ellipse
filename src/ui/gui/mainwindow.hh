#ifndef MAINWINDOW_HH_
# define MAINWINDOW_HH_

# include <QMainWindow>
# include "singleton.hh"

# define MAINWINDOW Singleton<MainWindow>::instance()

// forward decs
class QWidget;
class Filters;
class Layers;
class Parameters;
class Tabs;
class MenuBar;
class StatusBar;
class ToolBar;
class QCloseEvent;

class MainWindow : public QMainWindow
{
  Q_OBJECT;
public:
  MainWindow (QWidget* parent = 0);
  virtual void closeEvent (QCloseEvent*);
  ~MainWindow ();
};

#endif /* !MAINWINDOW_HH_ */
