#include "history.hh"
#include "history.moc"

#include <QWidget>
#include <QUndoView>
#include <QUndoGroup>
#include <QUndoStack>

History::History (QWidget* parent)
  : QDockWidget (parent)
{
  setWindowTitle ("History");
  setObjectName ("History");
  setAllowedAreas (Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
  setMinimumSize (100, 100);
  view_ = new QUndoView (this);
  group_ = new QUndoGroup (view_);
  view_->setGroup (group_);
  view_->setEmptyLabel ("New project");
  setWidget (view_);
}

History::~History ()
{
}

void History::push (QUndoCommand* command)
{
  group_->activeStack ()->push (command);
}

QUndoGroup* History::group () const
{
  return group_;
}

void History::newStack (int index)
{
  stacks_[index] = new QUndoStack (group_);
}

void History::changeActiveStack (int index)
{
  group_->setActiveStack (stacks_[index]);
}

void History::cancel ()
{
}
