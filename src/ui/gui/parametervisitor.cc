#include "parametervisitor.hh"
#include "colorwidget.hh"
#include "gradientwidget.hh"
#include <QGridLayout>
#include <QWidget>
#include <QCheckBox>
#include <QRadioButton>
#include <QSlider>
#include <QSpinBox>
#include <QLabel>
#include <iostream>
#include "preview.hh"

ParameterVisitor::ParameterVisitor (QWidget* parent,
				    QGridLayout* layout)
  : parent_ (parent),
    layout_ (layout),
    row_ (1)
{
}

namespace
{
  void linkToParent (Parameter* parent, QWidget* child)
  {
    if (parent)
    {
      if (!parent->value ())
	child->setEnabled (false);
      QObject::connect (parent, SIGNAL (valueChanged (bool)),
			child, SLOT (setEnabled (bool)));
    }
  }
} // anonymousœ

void ParameterVisitor::showDesc (const Parameter* param)
{
  if (param->description () != "")
  {
    int offset = 0;
    if (param->parent_)
      offset = 1;
    QLabel* label = new QLabel (parent_);
    layout_->addWidget (label, row_, offset, 1, 10 - offset);
    label->setText (param->description ());
    label->setWordWrap (true);
    linkToParent (param->parent_, label);
    row_++;
  }
}

void ParameterVisitor::lastRow ()
{
  layout_->addWidget (new QWidget (parent_), row_, 0, 1, 10);
  layout_->setRowMinimumHeight (row_, 0);
  layout_->setRowStretch (row_, 1);
}

void ParameterVisitor::visit (const CheckParameter* param)
{
  showDesc (param);
  int offset = 0;
  if (param->parent_)
    offset = 1;
  QCheckBox* check = new QCheckBox (parent_);
  layout_->addWidget (check, row_, offset, 1, 10 - offset);
  check->setText (param->text ());
  check->setChecked (param->value ());
  param->widgets_->push_back (check);
  row_++;
  linkToParent (param->parent_, check);
  QObject::connect (check, SIGNAL (toggled (bool)),
		    param, SLOT (changeValue (bool)));
  QObject::connect (param, SIGNAL (toggled (bool)),
		    check, SLOT (changeValue (bool)));
  QObject::connect (check, SIGNAL (toggled (bool)),
		    PREVIEW, SLOT (reload ()));
  check->setStatusTip (param->description ());
}

void ParameterVisitor::visit (const RadioParameter* param)
{
  showDesc (param);
  int offset = 0;
  if (param->parent_)
    offset = 1;
  QRadioButton* check = new QRadioButton (parent_);
  layout_->addWidget (check, row_, offset, 1, 10 - offset);
  check->setText (param->text ());
  check->setChecked (param->value ());
  param->widgets_->push_back (check);
  row_++;
  linkToParent (param->parent_, check);
  QObject::connect (check, SIGNAL (toggled (bool)),
		    param, SLOT (changeValue (bool)));
  QObject::connect (param, SIGNAL (toggled (bool)),
		    check, SLOT (changeValue (bool)));
  QObject::connect (check, SIGNAL (toggled (bool)),
		    PREVIEW, SLOT (reload ()));
  check->setStatusTip (param->description ());
}

void ParameterVisitor::visit (const SliderParameter* param)
{
  showDesc (param);
  int offset = 0;
  if (param->parent_)
    offset = 1;
  QSlider* slider = new QSlider (Qt::Horizontal, parent_);
  layout_->addWidget (slider, row_, offset, 1, 9 - offset);
  slider->setTickPosition (QSlider::TicksBelow);
  slider->setTickInterval (param->step () * 10);
  slider->setMinimum (param->minimum ());
  slider->setMaximum (param->maximum ());
  slider->setSingleStep (param->step ());
  slider->setValue (param->value ());
  linkToParent (param->parent_, slider);
  QSpinBox* spin = new QSpinBox (parent_);
  layout_->addWidget (spin, row_, 9, 1, 1);
  spin->setMinimum (param->minimum ());
  spin->setMaximum (param->maximum ());
  spin->setSingleStep (param->step ());
  spin->setValue (param->value ());
  linkToParent (param->parent_, spin);
  QObject::connect (slider, SIGNAL (valueChanged (int)),
		    spin, SLOT (setValue (int)));
  QObject::connect (spin, SIGNAL (valueChanged (int)),
		    slider, SLOT (setValue (int)));
  QObject::connect (slider, SIGNAL (valueChanged (int)),
		    param, SLOT (changeValue (int)));
  QObject::connect (param, SIGNAL (valueChanged (int)),
		    slider, SLOT (changeValue (int)));
  QObject::connect (slider, SIGNAL (valueChanged (int)),
		    PREVIEW, SLOT (reload ()));
  row_++;
  slider->setStatusTip (param->description ());
  spin->setStatusTip (param->description ());

}

void ParameterVisitor::visit (const ColorParameter* param)
{
  showDesc (param);
  int offset = 0;
  if (param->parent_)
    offset = 1;
  ColorWidget* color = new ColorWidget (parent_);
  layout_->addWidget (color, row_, offset, 1, 10 - offset);
  color->addColor (param->colors (), param->thresholds ());
  color->setMinimum (param->minimum ());
  color->setMaximum (param->maximum ());
  linkToParent (param->parent_, color);
  QObject::connect (color, SIGNAL (stateChanged (std::deque<QColor>&,
						 std::deque<int>&)),
		    param, SLOT (changeState (std::deque<QColor>&,
					      std::deque<int>&)));
  QObject::connect (param, SIGNAL (stateChanged (std::deque<QColor>&,
						 std::deque<int>&)),
		    color, SLOT (changeState (std::deque<QColor>&,
					      std::deque<int>&)));
  QObject::connect (color, SIGNAL (stateChanged (std::deque<QColor>&,
						 std::deque<int>&)),
		    PREVIEW, SLOT (reload ()));
  row_++;
  color->setStatusTip ("Drag cursors to move thresholds. "
		       "Click on colors to change them.");
}

void ParameterVisitor::visit (const GradientParameter* param)
{
  showDesc (param);
  int offset = 0;
  if (param->parent_)
    offset = 1;
  GradientWidget* color = new GradientWidget (parent_);
  layout_->addWidget (color, row_, offset, 1, 10 - offset);
  color->addColor (param->colors (), param->thresholds ());
  color->setMinimum (param->minimum ());
  color->setMaximum (param->maximum ());
  linkToParent (param->parent_, color);
  QObject::connect (color, SIGNAL (stateChanged (std::deque<QColor>&,
						 std::deque<int>&)),
		    param, SLOT (changeState (std::deque<QColor>&,
					      std::deque<int>&)));
  QObject::connect (param, SIGNAL (stateChanged (std::deque<QColor>&,
						 std::deque<int>&)),
		    color, SLOT (changeState (std::deque<QColor>&,
					      std::deque<int>&)));
  QObject::connect (color, SIGNAL (stateChanged (std::deque<QColor>&,
						 std::deque<int>&)),
		    PREVIEW, SLOT (reload ()));
  row_++;
  color->setStatusTip ("Drag cursors to move colors. "
		       "Double click on cursors to change colors. "
		       "Right click on cursors to delete colors. "
		       "Click anywhere to add a color.");
}
