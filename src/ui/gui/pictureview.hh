#ifndef PICTUREVIEW_HH_
# define PICTUREVIEW_HH_

# include <QWidget>
# include "singleton.hh"

# define PICVIEW Singleton<PictureView>::instance()

// forward decs
class QWidget;
class QImage;
class QPaintEvent;
class QWheelEvent;

class PictureView : public QWidget
{
  Q_OBJECT;
public:
  PictureView (QString& file, QWidget* parent = 0);
  ~PictureView ();
  bool good () const;
  QImage& image ();
public slots:
  void setZoom (float);
signals:
  void zoomChanged (float);
protected:
  void paintEvent (QPaintEvent*);
private:
  QImage* img_;
  QWidget* parent_;
  float zoom_;
};

#endif /* !PICTUREVIEW_HH_ */
