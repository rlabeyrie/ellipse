#include "applyfilter.hh"
#include "applyfilter.moc"

#include "plugin.hh"
#include "selection.hh"
#include "filters.hh"
#include "tabs.hh"
#include "history.hh"
#include "statusbar.hh"
#include "toolbar.hh"
#include "parameters.hh"
#include "toolbar.hh"
#include "preview.hh"

#include <QObject>
#include <QPushButton>
#include <QTimer>
#include <iostream>

ApplyFilter::ApplyFilter (Plugin* plugin, Selection* selection)
  : plugin_ (plugin),
    undoSelect_ (selection),
    redoSelect_ (0)
{
  setText (plugin->name ());
}

void ApplyFilter::undo ()
{
  FILTERS->patch (undoSelect_);
}

void ApplyFilter::redo ()
{
  if (redoSelect_)
  {
    FILTERS->patch (redoSelect_);
  }
  else if (!isRunning ())
  {
    redoSelect_ = new Selection (*undoSelect_);
    FILTERS->anticipatedPatch (redoSelect_);
    STATUS->showProgress ();
    TABS->setEnabled (false);
    TOOLBAR->setEnabled (false);
    FILTERS->setEnabled (false);
    PARAMS->setEnabled (false);
    HISTORY->setEnabled (false);
    QObject::connect (this, SIGNAL (finished ()),
		      FILTERS, SLOT (flushPatch ()));
    QObject::connect (this, SIGNAL (finished ()),
		      STATUS, SLOT (hideProgress ()));
    QObject::connect (this, SIGNAL (finished ()),
		      PREVIEW, SLOT (load ()));
    QObject::connect (this, SIGNAL (terminated ()),
		      STATUS, SLOT (hideProgress ()));
    QObject::connect (this, SIGNAL (terminated ()),
		      FILTERS, SLOT (cancel ()));
    QObject::connect (STATUS->cancel (), SIGNAL (clicked ()),
		      this, SLOT (terminate ()));
    STATUS->newTimer ();
    QObject::connect (STATUS->timer (), SIGNAL (timeout ()),
		      this, SLOT (refreshProgress ()));
    STATUS->timer ()->start (100);
    start (TimeCriticalPriority);
  }
}

void ApplyFilter::refreshProgress ()
{
  STATUS->refreshProgress (plugin_->progress);
}

ApplyFilter::~ApplyFilter ()
{
  delete undoSelect_;
  delete redoSelect_;
}

void ApplyFilter::run ()
{
  plugin_->progress = 0;
  plugin_->process (*undoSelect_, *redoSelect_);
  plugin_->histogram (*redoSelect_);
  STATUS->endTimer ();
}
