#ifndef TABS_HH_
# define TABS_HH_

# include <QTabWidget>
# include <map>
# include "singleton.hh"

# define TABS Singleton<Tabs>::instance()

// forward decs
class QWidget;
class PictureView;
class QScrollArea;
class QUndoStack;

class Tabs : public QTabWidget
{
  Q_OBJECT;
public:
  Tabs (QWidget* parent = 0);
  ~Tabs ();
public slots:
  void open ();
private:
  std::map<QScrollArea*, PictureView*> pics_;
};

#endif /* !TABS_HH_ */
