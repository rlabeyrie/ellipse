#ifndef LAYERS_HH_
# define LAYERS_HH_

# include <QDockWidget>
# include "singleton.hh"

# define LAYERS Singleton<Layers>::instance()

// forward decs
class QWidget;

class Layers : public QDockWidget
{
  Q_OBJECT;
public:
  ~Layers ();
  Layers (QWidget* parent = 0);
};

// template <>
// Layers* Singleton<Layers>::instance_ = 0;

#endif /* !LAYERS_HH_ */
