#include "preview.hh"
#include "preview.moc"
#include "previewwidget.hh"
#include "tabs.hh"
#include "pictureview.hh"

#include <QWidget>
#include <QScrollArea>
#include <iostream>

Preview::Preview (QWidget* parent)
  : QDockWidget (parent)
{
  setWindowTitle ("Preview");
  setObjectName ("Preview");
  setAllowedAreas (Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
  setMinimumSize (100, 100);
  view_ = new PreviewWidget (this);
  setWidget (view_);
}

Preview::~Preview ()
{
}

void Preview::load ()
{
  QWidget* current = TABS->currentWidget ();
  if (current)
  {
    QScrollArea* area = (QScrollArea*) (current);
    PictureView* view = (PictureView*) area->widget ();
    view_->load (view->image ());
  }
}

void Preview::reload ()
{
  view_->reload ();
}
