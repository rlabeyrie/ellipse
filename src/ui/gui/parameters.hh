#ifndef PARAMETERS_HH_
# define PARAMETERS_HH_

# include <QDockWidget>
# include "singleton.hh"

# define PARAMS Singleton<Parameters>::instance()

// forward decs
class QWidget;
class QLabel;
class QGridLayout;
class QScrollArea;

class Parameters : public QDockWidget
{
  Q_OBJECT;
public:
  Parameters (QWidget* parent = 0);
  ~Parameters ();
public slots:
  void reloadParameters ();
private:
  QWidget* content_;
};

#endif /* !PARAMETERS_HH_ */
