#include "pictureview.hh"
#include "pictureview.moc"

#include <QWidget>
#include <QImage>
#include <QFileDialog>
#include <QPainter>
#include <QString>
#include <iostream>

PictureView::PictureView (QString& file, QWidget* parent)
  : QWidget (parent)
{
  parent_ = parent;
  img_ = new QImage (file);
  zoom_ = 1;
  setMinimumSize (img_->width () * zoom_, img_->height () * zoom_);
}

PictureView::~PictureView ()
{
}

void PictureView::setZoom (float f)
{
  int w;
  int h;

  if (f == zoom_)
    return;
  zoom_ = f;
  emit (zoomChanged (zoom_));
  w = img_->width () * zoom_;
  h = img_->height () * zoom_;
  setMinimumSize (w,h);
  resize (parent_->width (), parent_->height ());
  repaint ();
}

void PictureView::paintEvent (QPaintEvent*)
{
  int xoffset;
  int yoffset;

  if (width () > img_->width() * zoom_)
  {
    xoffset = (width () - img_->width () * zoom_) / 2;
  }
  else
  {
    xoffset = 0;
  }
  if (height () > img_->height () * zoom_)
  {
    yoffset = (height () - img_->height () * zoom_) / 2;
  }
  else
  {
    yoffset = 0;
  }
  QPainter p (this);
  p.save ();
  p.translate (xoffset, yoffset);
  p.scale (zoom_, zoom_);
  p.drawImage (0, 0, *img_);
  p.restore ();
}

bool PictureView::good () const
{
  return !img_->isNull ();
}

QImage& PictureView::image ()
{
  return *img_;
}
