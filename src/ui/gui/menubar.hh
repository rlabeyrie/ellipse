#ifndef MENUBAR_HH_
# define MENUBAR_HH_

# include <QMenuBar>
# include "singleton.hh"

# define MENUBAR Singleton<MenuBar>::instance()

// forward decs
class QWidget;
class QMenuBar;
class QMenu;

class MenuBar : public QMenuBar
{
  Q_OBJECT;
public:
  ~MenuBar ();
  MenuBar (QWidget* parent = 0);
  void newMenu (QMenu*);
};

#endif /* !MENUBAR_HH_ */
