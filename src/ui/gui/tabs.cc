#include "tabs.hh"
#include "tabs.moc"
#include "pictureview.hh"
#include "history.hh"
#include "preview.hh"

#include <QWidget>
#include <QScrollArea>
#include <QString>
#include <QFileDialog>
#include <QUndoStack>
#include <QUndoGroup>
#include <iostream>
#include <QRegExp>

Tabs::Tabs (QWidget* parent)
  : QTabWidget (parent)
{
  setTabsClosable (true);
}

Tabs::~Tabs ()
{
}

void Tabs::open ()
{
  QScrollArea* area = new QScrollArea (this);
  QString file =
    QFileDialog::getOpenFileName(this,
				 tr("Open Image"),
				 ".",
				 tr("Image Files (*.png *.jpg *.bmp *.ppm *.gif)"));
  PictureView* pic = new PictureView (file, area);
  if (pic->good ())
  {
    area->setWidget (pic);
    area->setWidgetResizable (true);
    addTab (area, file.remove (QRegExp ("(.*/)+")));
    HISTORY->newStack (indexOf (area));
    setCurrentWidget (area);
    emit (currentChanged (currentIndex ()));
    pics_[area] = pic;
  }
  else
    delete area;
}
