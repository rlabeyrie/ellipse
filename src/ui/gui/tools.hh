#ifndef TOOLS_HH_
# define TOOLS_HH_

# include <QDockWidget>
# include "singleton.hh"

# define TOOLS Singleton<Tools>::instance()

// forward decs
class QWidget;

class Tools : public QDockWidget
{
  Q_OBJECT;
public:
  ~Tools ();
  Tools (QWidget* parent = 0);
};

#endif /* !TOOLS_HH_ */
