#include "gradientwidget.hh"
#include "gradientwidget.moc"

#include <QColor>
#include <QColorDialog>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QPainter>
#include <QPoint>
#include <QRect>
#include <QGradient>
#include <iostream>

#define MIN 6
#define MAX (width () - 6)
#define RANGE (width () - 12)

GradientWidget::GradientWidget (QWidget* parent)
  : QWidget (parent),
    activeCursor_ (-1),
    min_ (0),
    max_ (100),
    range_ (100)
{
  setAutoFillBackground (true);
  setMinimumHeight (20);
  setMinimumWidth (20);
}

inline
int GradientWidget::pixelToThreshold (int p) const
{
  return (((float) p - MIN) / RANGE) * range_ + min_;
}

inline
int GradientWidget::thresholdToPixel (int t) const
{
  return (((float) t - min_) / range_) * RANGE + MIN;
}

float GradientWidget::thresholdToFloat (int t) const
{
  return (((float) t - min_) / range_);
}

void GradientWidget::mouseMoveEvent (QMouseEvent* event)
{
  if (activeCursor_ >= 0)
  {
    int x = (event->x () < MIN) ? MIN :
      ((event->x () > MAX) ? MAX : event->x ());
    int t = pixelToThreshold (x);
    if (!(activeCursor_ > 0 &&
	 thresholds_[activeCursor_ - 1] >= t) &&
	!(((unsigned) activeCursor_ < thresholds_.size () - 1) &&
	  thresholds_[activeCursor_ + 1] <= t))
    {
      thresholds_[activeCursor_] = t;
    }
    repaint ();
    emit (thresholdsChanged (thresholds_));
    emit (stateChanged (colors_, thresholds_));
  }
}

void GradientWidget::mouseDoubleClickEvent (QMouseEvent* event)
{
  for (unsigned i = 0; i < cursors_.size (); ++i)
    if (cursors_[i].contains (event->pos ()))
    {
      colors_[i] = QColorDialog::getColor (colors_[i],
					   0,
					   "Select a color",
					   0);
      repaint ();
      emit (colorsChanged (colors_));
      emit (stateChanged (colors_, thresholds_));
      break;
    }
}

void GradientWidget::mousePressEvent (QMouseEvent* event)
{
  if (activeCursor_ < 0)
  {
    for (unsigned i = 0; i < cursors_.size () && activeCursor_ < 0; ++i)
      if (cursors_[i].contains (event->pos ()))
      {
	if (event->button () == Qt::RightButton)
	{
	  std::deque<QColor> c;
	  for (unsigned j = 0; j < colors_.size (); ++j)
	    if (j != i)
	      c.push_back (colors_[j]);
	  colors_ = c;
	  std::deque<int> t;
	  for (unsigned j = 0; j < thresholds_.size (); ++j)
	    if (j != i)
	      t.push_back (thresholds_[j]);
	  colors_ = c;
	  thresholds_ = t;
	  emit (colorsChanged (colors_));
	  emit (thresholdsChanged (thresholds_));
	  emit (stateChanged (colors_, thresholds_));
	}
	else
	  activeCursor_ = i;
      }
    repaint ();
  }
  if (activeCursor_ < 0)
  {
    QRect rect (MIN, 1, RANGE, height () - 8);
    if (rect.contains (event->pos ()))
    {
      colors_.push_back (QColorDialog::getColor ());
      thresholds_.push_back (pixelToThreshold (event->x ()));
      sort ();
      repaint ();
      emit (colorsChanged (colors_));
      emit (stateChanged (colors_, thresholds_));
    }
  }
}

void GradientWidget::sort ()
{
  while (sortRecurs ())
    ;
}

bool GradientWidget::sortRecurs ()
{
  bool changed = false;
  for (unsigned i = 0; i < thresholds_.size (); ++i)
  {
    if (i > 0 && thresholds_[i] < thresholds_[i - 1])
    {
      QColor temp1 = colors_[i];
      int temp2 = thresholds_[i];
      colors_[i] = colors_[i - 1];
      thresholds_[i] = thresholds_[i - 1];
      colors_[i - 1] = temp1;
      thresholds_[i - 1] = temp2;
      changed = true;
    }
  }
  return changed;
}

void GradientWidget::mouseReleaseEvent (QMouseEvent*)
{
  if (activeCursor_ >= 0)
  {
    activeCursor_ = -1;
    repaint ();
  }
}

void GradientWidget::paintEvent (QPaintEvent*)
{
  QPainter p (this);
  const QBrush& def = p.brush ();
  QBrush white (QColor (255, 255, 255));
  QBrush black (QColor (0, 0, 0));
  QBrush gray (QColor (100, 100, 100));
  p.drawRect (5, 0, width () - 11, height () - 7);
  zones_.clear ();
  QLinearGradient gradient (MIN, 1, MIN + RANGE, 1);
  for (unsigned i = 0; i < colors_.size (); ++i)
  {
    float f1 = thresholdToFloat (thresholds_[i]);
    gradient.setColorAt (f1, colors_[i]);
  }
  QBrush brush (gradient);
  p.fillRect (MIN, 1, RANGE, height () - 8, brush);
  cursors_.clear ();
  for (unsigned i = 0; i < colors_.size (); ++i)
  {
    unsigned x = thresholdToPixel (thresholds_[i]);
    QPoint points[] =
      {
	QPoint (x, height () - 15),
	QPoint (x + 3, height () - 7),
	QPoint (x + 3, height () - 1),
	QPoint (x - 3, height () - 1),
	QPoint (x - 3, height () - 7)
      };
    cursors_.push_back (QRect (x - 3, height () - 15, 8, 15));
    if (activeCursor_ == (int) i)
      p.setBrush (gray);
    else
      p.setBrush (colors_[i]);
    p.drawPolygon (points, 5);
    p.setBrush (def);
  }
}

void GradientWidget::addColor (QColor c, int t)
{
  colors_.push_back (c);
  thresholds_.push_back (t);
}

void GradientWidget::addColor (std::deque<QColor> c, std::deque<int> t)
{
  colors_ = c;
  thresholds_ = t;
}

void GradientWidget::setMinimum (int m)
{
  min_ = m;
  range_ = (max_ - min_);
}

void GradientWidget::setMaximum (int m)
{
  max_ = m;
  range_ = (max_ - min_);
}

std::deque<QColor>& GradientWidget::colors ()
{
  return colors_;
}

std::deque<int>& GradientWidget::thresholds ()
{
  return thresholds_;
}
