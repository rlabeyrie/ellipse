#ifndef COLORWIDGET_HH_
# define COLORWIDGET_HH_

# include <QWidget>
# include <deque>

// forward decs
class QColor;
class QPaintEvent;
class QMouseEvent;
class QRect;

class ColorWidget : public QWidget
{
  Q_OBJECT;
public:
  ColorWidget (QWidget* parent = 0);
  virtual void mouseMoveEvent (QMouseEvent*);
  virtual void mousePressEvent (QMouseEvent*);
  virtual void mouseReleaseEvent (QMouseEvent*);
  virtual void paintEvent (QPaintEvent*);
  void addColor (QColor&, int);
  void addColor (std::deque<QColor>, std::deque<int>);
  void setMinimum (int);
  void setMaximum (int);
  std::deque<QColor>& colors ();
  std::deque<int>& thresholds ();
signals:
  void colorsChanged (std::deque<QColor>&);
  void thresholdsChanged (std::deque<int>&);
  void stateChanged (std::deque<QColor>&, std::deque<int>&);
private:
  int pixelToThreshold (int) const;
  int thresholdToPixel (int) const;
private:
  std::deque<QColor> colors_;
  std::deque<int> thresholds_;
  std::deque<QRect> cursors_;
  std::deque<QRect> zones_;
  int activeCursor_;
  int min_;
  int max_;
  int range_;
};

#endif /* !COLORWIDGET_HH_ */
