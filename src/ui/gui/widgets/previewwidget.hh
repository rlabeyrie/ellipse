#ifndef PREVIEWWIDGET_HH_
# define PREVIEWWIDGET_HH_

# include <QWidget>

// forward decs
class QImage;
class QPaintEvent;
class Selection;
class QResizeEvent;

class PreviewWidget : public QWidget
{
  Q_OBJECT;
public:
  PreviewWidget (QWidget* parent = 0);
  virtual void paintEvent (QPaintEvent*);
  virtual void resizeEvent (QResizeEvent*);
public slots:
  void load (QImage&);
  void reload ();
private:
  QImage* map_;
  Selection* scaled_;
  Selection* final_;
  QImage* show_;
};

#endif /* !PREVIEWWIDGET_HH_ */
