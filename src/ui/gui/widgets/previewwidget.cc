#include "previewwidget.hh"
#include "previewwidget.moc"
#include "filters.hh"
#include "selection.hh"

#include <QPainter>
#include <iostream>

PreviewWidget::PreviewWidget (QWidget* parent)
  : QWidget (parent),
    map_ (0),
    scaled_ (0),
    final_ (0),
    show_ (0)
{
  setAutoFillBackground (true);
}

void PreviewWidget::load (QImage& img)
{
  if (scaled_)
    delete scaled_;
  if (show_)
    delete show_;
  map_ = &img;
  float ratio1 = ((float) width ()) / height ();
  float ratio2 = ((float) img.width ()) / img.height ();
  float ratio;
  int w;
  int h;
  if (ratio1 <= ratio2)
  {
    ratio = (float) width () / img.width ();
    w = width ();
    h = img.height () * ratio;
  }
  else
  {
    ratio = (float) height () / img.height ();
    w = img.width () * ratio;
    h = height ();
  }
  scaled_ = new Selection (img, ratio);
  show_ = new QImage (w, h, QImage::Format_RGB32);
  reload ();
}

void PreviewWidget::reload ()
{
  if (show_)
  {
    if (final_)
      delete final_;
    final_ = new Selection (*scaled_);
    FILTERS->process (scaled_, final_);
    for (int i = 0; i < show_->height (); ++i)
      for (int j = 0; j < show_->width (); ++j)
      {
      show_->setPixel (j, i, (*final_)[i][j]);
      }
    repaint ();
  }
}

void PreviewWidget::paintEvent (QPaintEvent*)
{
  QPainter painter (this);
  if (show_)
    painter.drawImage (width () / 2 - show_->width () / 2,
		       height () / 2 - show_->height () / 2,
		       *show_);
}

void PreviewWidget::resizeEvent (QResizeEvent*)
{
  // if (map_)
  //   load (*map_);
}
