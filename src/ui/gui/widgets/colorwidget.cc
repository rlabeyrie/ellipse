#include "colorwidget.hh"
#include "colorwidget.moc"

#include <QColor>
#include <QColorDialog>
#include <QPaintEvent>
#include <QMouseEvent>
#include <QPainter>
#include <QPoint>
#include <QRect>
#include <iostream>

#define MIN 6
#define MAX (width () - 6)
#define RANGE (width () - 12)

ColorWidget::ColorWidget (QWidget* parent)
  : QWidget (parent),
    activeCursor_ (-1),
    min_ (0),
    max_ (100),
    range_ (100)
{
  setAutoFillBackground (true);
  setMinimumHeight (20);
  setMinimumWidth (20);
}

inline
int ColorWidget::pixelToThreshold (int p) const
{
  return (((float) p - MIN) / RANGE) * range_ + min_;
}

inline
int ColorWidget::thresholdToPixel (int t) const
{
  return (((float) t - min_) / range_) * RANGE + MIN;
}

void ColorWidget::mouseMoveEvent (QMouseEvent* event)
{
  if (activeCursor_ >= 0)
  {
    int x = (event->x () < MIN) ? MIN :
      ((event->x () > MAX) ? MAX : event->x ());
    int t = pixelToThreshold (x);
    if (!(activeCursor_ > 0 &&
	 thresholds_[activeCursor_ - 1] > t) &&
	!(((unsigned) activeCursor_ < thresholds_.size () - 1) &&
	  thresholds_[activeCursor_ + 1] < t))
    {
      thresholds_[activeCursor_] = t;
    }
    repaint ();
    emit (thresholdsChanged (thresholds_));
    emit (stateChanged (colors_, thresholds_));
  }
}

void ColorWidget::mousePressEvent (QMouseEvent* event)
{
  if (activeCursor_ < 0)
  {
    for (unsigned i = 0; i < cursors_.size () && activeCursor_ < 0; ++i)
      if (cursors_[i].contains (event->pos ()))
	activeCursor_ = i;
    repaint ();
  }
  if (activeCursor_ < 0)
  {
    for (unsigned i = 0; i < zones_.size (); ++i)
      if (zones_[i].contains (event->pos ()))
	colors_[i] = QColorDialog::getColor (colors_[i],
					     0,
					     "Select a color",
					     0);
    repaint ();
    emit (colorsChanged (colors_));
    emit (stateChanged (colors_, thresholds_));
  }
}

void ColorWidget::mouseReleaseEvent (QMouseEvent*)
{
  if (activeCursor_ >= 0)
  {
    activeCursor_ = -1;
    repaint ();
  }
}

void ColorWidget::paintEvent (QPaintEvent*)
{
  QPainter p (this);
  const QBrush& def = p.brush ();
  QBrush white (QColor (255, 255, 255));
  QBrush black (QColor (0, 0, 0));
  QBrush gray (QColor (100, 100, 100));
  p.drawRect (5, 0, width () - 11, height () - 7);
  unsigned offset = MIN;
  zones_.clear ();
  for (unsigned i = 0; i < colors_.size (); ++i)
  {
    unsigned x = thresholdToPixel (thresholds_[i]);
    QRect rect (offset, 1, x - offset, height () - 8);
    if (!((i > 0 && thresholds_[i - 1] == thresholds_[i]) ||
	  (i == 0 && thresholds_[i] == min_)))
      p.fillRect (rect, colors_[i]);
    zones_.push_back (rect);
    offset = x + 1;
  }
  cursors_.clear ();
  for (unsigned i = 0; i < colors_.size (); ++i)
  {
    if (i < colors_.size () - 1)
    {
      unsigned x = thresholdToPixel (thresholds_[i]);
      QPoint points[] =
	{
	  QPoint (x, height () - 15),
	  QPoint (x + 3, height () - 7),
	  QPoint (x + 3, height () - 1),
	  QPoint (x - 3, height () - 1),
	  QPoint (x - 3, height () - 7)
	};
      cursors_.push_back (QRect (x - 3, height () - 15, 8, 15));
      if (activeCursor_ == (int) i)
	p.setBrush (gray);
      else
	p.setBrush (white);
      p.drawPolygon (points, 5);
      p.setBrush (def);
    }
  }
}

void ColorWidget::addColor (QColor& c, int t)
{
  colors_.push_back (c);
  thresholds_.push_back (t);
}

void ColorWidget::addColor (std::deque<QColor> c, std::deque<int> t)
{
  colors_ = c;
  thresholds_ = t;
}

void ColorWidget::setMinimum (int m)
{
  min_ = m;
  range_ = (max_ - min_);
}

void ColorWidget::setMaximum (int m)
{
  max_ = m;
  range_ = (max_ - min_);
}

std::deque<QColor>& ColorWidget::colors ()
{
  return colors_;
}

std::deque<int>& ColorWidget::thresholds ()
{
  return thresholds_;
}
