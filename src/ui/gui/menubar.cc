#include "menubar.hh"
#include "menubar.moc"
#include "mainwindow.hh"
#include "history.hh"

#include <QWidget>
#include <QMenu>
#include <QUndoGroup>
#include <QAction>

MenuBar::MenuBar (QWidget* parent)
  : QMenuBar (parent)
{
  addMenu ("File");
  QMenu* edit = addMenu ("Edit");
  QAction* undo = HISTORY->group ()->createUndoAction (this);
  undo->setShortcut (QKeySequence("Ctrl+Z"));
  edit->addAction (undo);
  QAction* redo = HISTORY->group ()->createRedoAction (this);
  redo->setShortcut (QKeySequence("Ctrl+Shift+Z"));
  edit->addAction (redo);
}

MenuBar::~MenuBar ()
{
}

void MenuBar::newMenu (QMenu* menu)
{
  addMenu (menu);
}
