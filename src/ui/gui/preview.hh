#ifndef PREVIEW_HH_
# define PREVIEW_HH_

# include <QDockWidget>
# include "singleton.hh"

# define PREVIEW Singleton<Preview>::instance()

// forward decs
class QWidget;
class PreviewWidget;
class QImage;

class Preview : public QDockWidget
{
  Q_OBJECT;
public:
  ~Preview ();
  Preview (QWidget* parent = 0);
public slots:
  void load ();
  void reload ();
private:
  PreviewWidget* view_;
};

#endif /* !PREVIEW_HH_ */
