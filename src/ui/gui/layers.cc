#include "layers.hh"
#include "layers.moc"

#include <QWidget>

Layers::Layers (QWidget* parent)
  : QDockWidget (parent)
{
  setWindowTitle ("Layers");
  setObjectName ("Layers");
  setAllowedAreas (Qt::LeftDockWidgetArea | Qt::RightDockWidgetArea);
  setMinimumSize (100, 100);
}

Layers::~Layers ()
{
}
