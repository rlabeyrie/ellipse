#ifndef FILTERS_HH_
# define FILTERS_HH_

# include <QDockWidget>
# include <deque>
# include <map>
# include "singleton.hh"

# define FILTERS Singleton<Filters>::instance()

// forward decs
class QWidget;
class QTreeWidget;
class QTreeWidgetItem;
class QString;
class QLabel;
class Plugin;
class Selection;
class ApplyFilter;

class Filters : public QDockWidget
{
  Q_OBJECT;
public:
  ~Filters ();
  Filters (QWidget* parent = 0);
  void treeLevelUp (QString);
  void treeLevelDown ();
  void newEntry (Plugin*);
  Plugin* current () const;
public slots:
  void emitFilterChanged (QTreeWidgetItem*, QTreeWidgetItem*);
  void process ();
  void process (const Selection*, Selection*);
  void patch (Selection* s);
  void anticipatedPatch (Selection* s);
  void flushPatch ();
  void cancel ();
signals:
  void filterChanged ();
private:
  QTreeWidget* tree_;
  std::deque<QTreeWidgetItem*> levels_;
  std::map<QTreeWidgetItem*, Plugin*> table_;
  Plugin* activeFilter_;
  QLabel* desc_;
  Selection* tempselect_;
  ApplyFilter* apply_;
};

#endif /* !FILTERS_HH_ */
