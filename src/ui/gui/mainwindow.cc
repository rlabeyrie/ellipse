#include "mainwindow.hh"
#include "mainwindow.moc"
#include "filters.hh"
#include "layers.hh"
#include "parameters.hh"
#include "tabs.hh"
#include "menubar.hh"
#include "statusbar.hh"
#include "toolbar.hh"
#include "history.hh"
#include "preview.hh"
#include "loader.hh"
#include "tools.hh"

#include <QSettings>
#include <QWidget>
#include <QMenu>
#include <iostream>
#include <QSplashScreen>

MainWindow::MainWindow (QWidget* parent)
  : QMainWindow (parent)
{
  setWindowTitle ("Ellipse Project - v0.5a");
  setGeometry (0, 0, 800, 600);
  setDockOptions (AnimatedDocks | AllowNestedDocks);

  addDockWidget (Qt::LeftDockWidgetArea,
		 Singleton<Tools>::newInstance (this));
  addDockWidget (Qt::LeftDockWidgetArea,
		 Singleton<Filters>::newInstance (this));
  addDockWidget (Qt::LeftDockWidgetArea,
		 Singleton<Parameters>::newInstance (this));
  addDockWidget (Qt::LeftDockWidgetArea,
  		 Singleton<Preview>::newInstance (this));
  addDockWidget (Qt::RightDockWidgetArea,
		 Singleton<History>::newInstance (this));
  addDockWidget (Qt::RightDockWidgetArea,
		 Singleton<Layers>::newInstance (this));
  setCentralWidget (Singleton<Tabs>::newInstance (this));
  addToolBar (Singleton<ToolBar>::newInstance (this));
  setStatusBar (Singleton<StatusBar>::newInstance (this));
  setMenuBar (Singleton<MenuBar>::newInstance (this));

  QMenu* menu = createPopupMenu ();
  menu->setTitle ("View");
  MENUBAR->newMenu (menu);
  QObject::connect (FILTERS, SIGNAL (filterChanged ()),
		    PARAMS, SLOT (reloadParameters ()));
  QObject::connect (TABS, SIGNAL (currentChanged (int)),
  		    HISTORY, SLOT (changeActiveStack (int)));
  QObject::connect (TABS, SIGNAL (currentChanged (int)),
  		    PREVIEW, SLOT (load ()));
  QObject::connect (FILTERS, SIGNAL (filterChanged ()),
		    PREVIEW, SLOT (reload ()));

  QSettings settings("Ellipse", "Ellipse");
  restoreGeometry(settings.value("MainWindow/geometry").toByteArray());
  restoreState(settings.value("MainWindow/windowState").toByteArray());
  LOADER->load ("./plugins");
}

MainWindow::~MainWindow ()
{
}

void MainWindow::closeEvent (QCloseEvent* event)
{
  QSettings settings("Ellipse", "Ellipse");
  settings.setValue("geometry", saveGeometry());
  settings.setValue("windowState", saveState());
  QMainWindow::closeEvent (event);
}
