#ifndef LOADER_HH_
# define LOADER_HH_

# include <QObject>
# include <deque>
# include "plugin.hh"

# include "singleton.hh"
# define LOADER Singleton<Loader>::instance()

// forward decs
class QString;

typedef Plugin* (*maker_plugin)();

class Loader : public QObject
{
  Q_OBJECT;
public:
  Loader (void*);
  void load (QString);
  void newMaker (maker_plugin);
private:
  maker_plugin lastPlugin_;
  unsigned id_;
};

#endif /* !LOADER_HH_ */
