#include "loader.hh"
#include "filters.hh"
#include "loader.moc"
#include "plugin.hh"
#include <dlfcn.h>
#include <deque>
#include <QFile>
#include <QDir>
#include <QString>
#include <QStringList>
#include <QErrorMessage>
#include <iostream>

maker_plugin g_maker;

Loader::Loader (void*)
  : id_ (0)
{
  if (!QDir ().exists ("./plugins"))
    QErrorMessage ().showMessage (
      "Warning: The `plugins' folder is not present at the root of"
      "the project.\n"
      "No plugins will be loaded at startup, but you can still load plugins"
      "manually.\n"
      );
}

void Loader::load (QString file)
{
  QDir dir (file);
  if (!dir.exists ())
  {
    if (file.endsWith (".so"))
    {
      void* lib = dlopen (file.toStdString ().c_str (), RTLD_LAZY);
      if (lib == NULL)
      {
	std::cout << dlerror () << std::endl;
	QErrorMessage ().showMessage (
	  "Error: Failed to load plugin %s.\n",
	  file
	  );
      }
      else
      {
	FILTERS->newEntry (g_maker ());
      }
    }
  }
  else if (!file.endsWith ("."))
  {
    if (file != "./plugins")
    {
      QFile group (dir.absolutePath () + "/group.txt");
      if (group.exists ())
	FILTERS->treeLevelUp (group.readAll ());
      else
	FILTERS->treeLevelUp (dir.canonicalPath ());
    }
    QStringList list = dir.entryList ();
    QStringList::iterator it;
    for (it = list.begin(); it != list.end();
	 ++it)
      load (dir.absolutePath () + "/" + *it);
    FILTERS->treeLevelDown ();
  }
}

void Loader::newMaker (maker_plugin maker)
{
  lastPlugin_ = maker;
}
