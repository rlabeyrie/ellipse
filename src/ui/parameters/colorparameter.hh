#ifndef COLORPARAMETER_HH_
# define COLORPARAMETER_HH_

# include "parameter.hh"
# include <deque>

// forward decs
class QColor;

class ColorParameter : public Parameter
{
  Q_OBJECT;
public:
  ColorParameter (Parameter* parent = 0);
  virtual void accept (ParameterVisitor&) const;
  void setMinimum (int);
  void setMaximum (int);
  int value () const;
  int maximum () const;
  int minimum () const;
  std::deque<QColor> colors () const;
  std::deque<int> thresholds () const;
  void addColor (QColor, int);
  int red () const;
  int green () const;
  int blue () const;
public slots:
  void changeState (std::deque<QColor>&, std::deque<int>&);
  void changeColors (std::deque<QColor>&);
  void changeThresholds (std::deque<int>&);
private:
  int value_;
  int min_;
  int max_;
  int step_;
  std::deque<QColor> colors_;
  std::deque<int> thresholds_;
};

# include "colorparameter.hxx"

#endif /* COLORPARAMETER_HH_ */
