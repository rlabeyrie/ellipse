#include "colorparameter.hh"
#include "colorparameter.moc"
#include "parametervisitor.hh"

void ColorParameter::accept (ParameterVisitor& visitor) const
{
  visitor.visit (this);
}

void ColorParameter::changeState (std::deque<QColor>& c, std::deque<int>& t)
{
  colors_ = c;
  thresholds_ = t;
}

void ColorParameter::changeColors (std::deque<QColor>& c)
{
  colors_ = c;
}

void ColorParameter::changeThresholds (std::deque<int>& t)
{
  thresholds_ = t;
}
