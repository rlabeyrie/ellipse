#include "gradientparameter.hh"
#include "gradientparameter.moc"
#include "parametervisitor.hh"

void GradientParameter::accept (ParameterVisitor& visitor) const
{
  visitor.visit (this);
}

void GradientParameter::changeState (std::deque<QColor>& c, std::deque<int>& t)
{
  colors_ = c;
  thresholds_ = t;
}

void GradientParameter::changeColors (std::deque<QColor>& c)
{
  colors_ = c;
}

void GradientParameter::changeThresholds (std::deque<int>& t)
{
  thresholds_ = t;
}
