#include "sliderparameter.hh"
#include "sliderparameter.moc"
#include "parametervisitor.hh"

void SliderParameter::accept (ParameterVisitor& visitor) const
{
  visitor.visit (this);
}

void SliderParameter::changeValue (int val)
{
  value_ = val;
}
