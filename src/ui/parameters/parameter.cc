#include "parameter.hh"
#include "parameter.moc"
#include "parametervisitor.hh"

void Parameter::changeValue (bool val)
{
  value_ = val;
  emit (valueChanged (val));
}
