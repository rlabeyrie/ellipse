#ifndef GRADIENTPARAMETER_HXX_
# define GRADIENTPARAMETER_HXX_

# include <QColor>
# include <iostream>

inline
GradientParameter::GradientParameter (Parameter* parent)
  : Parameter (parent),
    min_ (0),
    max_ (100),
    step_ (1)
{
}

inline
void GradientParameter::setMinimum (int i)
{
  min_ = i;
}

inline
void GradientParameter::setMaximum (int i)
{
  max_ = i;
}

inline
int GradientParameter::minimum () const
{
  return min_;
}

inline
int GradientParameter::maximum () const
{
  return max_;
}

inline
std::deque<QColor> GradientParameter::colors () const
{
  return colors_;
}

inline
std::deque<int> GradientParameter::thresholds () const
{
  return thresholds_;
}

inline
void GradientParameter::addColor (QColor color, int t)
{
  colors_.push_back (color);
  thresholds_.push_back (t);
}

inline
int GradientParameter::rgb (float f)
{
  for (unsigned i = 0; i < colors_.size (); ++i)
  {
    if (f <= thresholds_[i])
    {
      if (i == 0)
	return colors_[i].rgb ();
      float range = thresholds_[i] - thresholds_[i - 1];
      float fact1 = 1.f - ((float) thresholds_[i] - f) / range;
      float fact2 = 1.f - ((float) f - thresholds_[i - 1]) / range;
      int r = (colors_[i].red () * fact1 + colors_[i - 1].red () * fact2);
      int g = (colors_[i].green () * fact1 + colors_[i - 1].green () * fact2);
      int b = (colors_[i].blue () * fact1 + colors_[i - 1].blue () * fact2);
      return r * 65536 + g * 256 + b;
    }
  }
  return colors_.back ().rgb ();
}

inline
std::deque<unsigned> GradientParameter::buildGradient (unsigned s)
{
  std::deque<unsigned> ret;
  unsigned c = 0;
  float prev = 0;
  for (unsigned i = 0; i < thresholds_.size (); ++i)
  {
    float t = ((thresholds_[i] - (float) min_) / (max_ - min_)) * s;
    float range = t - prev;
    for (; c < t; ++c)
    {
      if (i == 0)
	ret.push_back (colors_[i].red () * 65536 +
		       colors_[i].green () * 256 +
		       colors_[i].blue ());
      else
      {
	float fact1 = 1.f - ((float) t - c) / range;
	float fact2 = 1.f - ((float) c - prev) / range;
	int r = colors_[i].red () * fact1 + colors_[i - 1].red () * fact2;
	int g = colors_[i].green () * fact1 + colors_[i - 1].green () * fact2;
	int b = colors_[i].blue () * fact1 + colors_[i - 1].blue () * fact2;
	ret.push_back (r * 65536 + g * 256 + b);
      }
    }
    prev = t;
  }
  if (thresholds_.size ())
    for (; c < s; ++c)
      ret.push_back (colors_.back ().red () * 65536
  		     + colors_.back ().green () * 256
  		     + colors_.back ().blue ());
  else
    for (; c < s; ++c)
      ret.push_back (c * 256 / s * 65536 + c * 256 / s * 256 + c * 256 / s);
  return ret;
}

#endif /* !GRADIENTPARAMETER_HXX_ */
