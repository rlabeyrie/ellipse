#ifndef GRADIENTPARAMETER_HH_
# define GRADIENTPARAMETER_HH_

# include "parameter.hh"
# include <deque>

// forward decs
class QColor;

class GradientParameter : public Parameter
{
  Q_OBJECT;
public:
  GradientParameter (Parameter* parent = 0);
  virtual void accept (ParameterVisitor&) const;
  void setMinimum (int);
  void setMaximum (int);
  int value () const;
  int maximum () const;
  int minimum () const;
  std::deque<QColor> colors () const;
  std::deque<int> thresholds () const;
  void addColor (QColor, int);
  int rgb (float);
  std::deque<unsigned> buildGradient (unsigned);
public slots:
  void changeState (std::deque<QColor>&, std::deque<int>&);
  void changeColors (std::deque<QColor>&);
  void changeThresholds (std::deque<int>&);
private:
  int min_;
  int max_;
  int step_;
  std::deque<QColor> colors_;
  std::deque<int> thresholds_;
};

# include "gradientparameter.hxx"

#endif /* GRADIENTPARAMETER_HH_ */
