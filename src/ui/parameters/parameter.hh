#ifndef PARAMETER_HH_
# define PARAMETER_HH_

# include <QString>
# include <QObject>
# include <QWidget>
# include <deque>

// forward decs
class ParameterVisitor;

class Parameter : public QObject
{
  Q_OBJECT;
public:
  Parameter (Parameter* parent = 0);
  bool value () const;
  void setValue (bool);
  void setText (const QString);
  const QString& text () const;
  void setDescription (QString desc);
  const QString& description () const;
  virtual void accept (ParameterVisitor&) const = 0;
  friend class ParameterVisitor;
public slots:
  void changeValue (bool);
signals:
  void valueChanged (bool);
private:
  QString description_;
  QString text_;
  Parameter* parent_;
  bool value_;
  std::deque<QWidget*>* widgets_;
};

# include "checkparameter.hh"
# include "radioparameter.hh"
# include "sliderparameter.hh"
# include "colorparameter.hh"
# include "gradientparameter.hh"

# include "parameter.hxx"

#endif /* !PARAMETER_HH_ */
