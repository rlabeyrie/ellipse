#ifndef RADIOPARAMETER_HH_
# define RADIOPARAMETER_HH_

# include "parameter.hh"

class RadioParameter : public Parameter
{
public:
  RadioParameter (Parameter* parent = 0);
  virtual void accept (ParameterVisitor&) const;
};

# include "radioparameter.hxx"

#endif /* RADIOPARAMETER_HH_ */
