#ifndef SLIDERPARAMETER_HH_
# define SLIDERPARAMETER_HH_

# include "parameter.hh"

class SliderParameter : public Parameter
{
  Q_OBJECT;
public:
  SliderParameter (Parameter* parent = 0);
  virtual void accept (ParameterVisitor&) const;
  void setValue (int);
  void setMinimum (int);
  void setMaximum (int);
  void setStep (int);
  int value () const;
  int maximum () const;
  int minimum () const;
  int step () const;
public slots:
  void changeValue (int);
private:
  int value_;
  int min_;
  int max_;
  int step_;
};

# include "sliderparameter.hxx"

#endif /* SLIDERPARAMETER_HH_ */
