#include "checkparameter.hh"
#include "parametervisitor.hh"

void CheckParameter::accept (ParameterVisitor& visitor) const
{
  visitor.visit (this);
}
