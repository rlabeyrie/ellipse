#ifndef PARAMETER_HXX_
# define PARAMETER_HXX_

inline
Parameter::Parameter (Parameter* parent)
  : parent_ (parent),
    value_ (false)
{
  widgets_ = new std::deque<QWidget*> ();
}

inline
void Parameter::setDescription (QString dec)
{
  description_ = dec;
}

inline
const QString& Parameter::description () const
{
  return description_;
}

inline
const QString& Parameter::text () const
{
  return text_;
}

inline
void Parameter::setText (const QString str)
{
  text_ = str;
}

inline
bool Parameter::value () const
{
  return value_;
}

inline
void Parameter::setValue (bool val)
{
  value_ = val;
  emit (valueChanged (val));
}

#endif /* !PARAMETER_HXX_ */
