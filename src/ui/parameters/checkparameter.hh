#ifndef CHECKPARAMETER_HH_
# define CHECKPARAMETER_HH_

# include "parameter.hh"

class CheckParameter : public Parameter
{
public:
  CheckParameter (Parameter* parent = 0);
  virtual void accept (ParameterVisitor&) const;
};

# include "checkparameter.hxx"

#endif /* CHECKPARAMETER_HH_ */
