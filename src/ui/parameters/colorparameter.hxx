#ifndef COLORPARAMETER_HXX_
# define COLORPARAMETER_HXX_

inline
ColorParameter::ColorParameter (Parameter* parent)
  : Parameter (parent),
    value_ (50),
    min_ (0),
    max_ (100),
    step_ (1)
{
}

inline
void ColorParameter::setMinimum (int i)
{
  min_ = i;
}

inline
void ColorParameter::setMaximum (int i)
{
  max_ = i;
}

inline
int ColorParameter::minimum () const
{
  return min_;
}

inline
int ColorParameter::maximum () const
{
  return max_;
}

inline
std::deque<QColor> ColorParameter::colors () const
{
  return colors_;
}

inline
std::deque<int> ColorParameter::thresholds () const
{
  return thresholds_;
}

inline
void ColorParameter::addColor (QColor color, int t)
{
  colors_.push_back (color);
  thresholds_.push_back (t);
}

inline
int ColorParameter::red () const
{
  return colors_[0].red ();
}

inline
int ColorParameter::green () const
{
  return colors_[0].green ();
}

inline
int ColorParameter::blue () const
{
  return colors_[0].blue ();
}

#endif /* !SLIDERPARAMETER_HXX_ */
