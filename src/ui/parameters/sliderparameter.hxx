#ifndef SLIDERPARAMETER_HXX_
# define SLIDERPARAMETER_HXX_

inline
SliderParameter::SliderParameter (Parameter* parent)
  : Parameter (parent),
    value_ (50),
    min_ (0),
    max_ (100),
    step_ (1)
{
}

inline
void SliderParameter::setValue (int i)
{
  value_ = i;
}

inline
void SliderParameter::setMinimum (int i)
{
  min_ = i;
}

inline
void SliderParameter::setMaximum (int i)
{
  max_ = i;
}

inline
void SliderParameter::setStep (int i)
{
  step_ = i;
}

inline
int SliderParameter::value () const
{
  return value_;
}

inline
int SliderParameter::minimum () const
{
  return min_;
}

inline
int SliderParameter::maximum () const
{
  return max_;
}

inline
int SliderParameter::step () const
{
  return step_;
}

#endif /* !SLIDERPARAMETER_HXX_ */
