#include "radioparameter.hh"
#include "parametervisitor.hh"

void RadioParameter::accept (ParameterVisitor& visitor) const
{
  visitor.visit (this);
}
