#include "main.hh"
#include <iostream>

int main (int argc, char** argv)
{
  QApplication app (argc, argv);
  MAINWINDOW->showMaximized ();
  return app.exec ();
}
