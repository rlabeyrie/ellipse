#ifndef SINGLETON_HH_
# define SINGLETON_HH_

// forward decs
class QWidget;

template <class T>
class Singleton
{
public:
  static T* newInstance (QWidget* parent = 0);
  static T* instance ();
private:
  Singleton ();
  Singleton (Singleton<T>& rhs);
  Singleton<T>& operator= (Singleton<T>& rhs);
private:
  static T* instance_;
};

# include "singleton.hxx"

#endif /* !SINGLETON_HH_ */
