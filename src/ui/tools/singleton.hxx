#ifndef SINGLETON_HXX_
# define SINGLETON_HXX_

template<class T>
inline
T* Singleton<T>::newInstance (QWidget* parent)
{
  if (instance_)
    return 0;
  instance_ = new T (parent);
  return instance_;
}

template<class T>
inline
T* Singleton<T>::instance ()
{
  if (!instance_)
    return newInstance ();
  return instance_;
}

#endif /* !SINGLETON_HXX_ */
