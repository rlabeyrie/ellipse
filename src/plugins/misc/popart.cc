#include "popart.hh"

ELLIPSE_PLUGIN(Popart);

Popart::Popart ()
{
  auto_ = new CheckParameter ();
  auto_->setValue (true);
  auto_->setText ("Auto thresholds");
  c_ = new ColorParameter (auto_);
  c_->setMinimum (0);
  c_->setMaximum (255);
  c_->addColor (QColor (255, 0, 0), 85);
  c_->addColor (QColor (0, 255, 0), 170);
  c_->addColor (QColor (0, 0, 255), 255);
  c_->setDescription ("Select the colors and thresholds");
  addParameter (auto_);
  addParameter (c_);
}

void Popart::process (const Selection&, Selection& sel)
{
  unsigned t1 = c_->thresholds ()[0];
  unsigned t2 = c_->thresholds ()[1];
  if (!auto_->value ())
  {
    unsigned occur[766];
    for (int i = 0; i <= 765; ++i)
      occur[i] = 0;
    for (unsigned y = 0; y < sel.height (); y++)
      for (unsigned x = 0; x < sel.width (); x++)
      {
	unsigned val = sel[y][x];
	unsigned i = ((val >> 16) % 256) + ((val >> 8) % 256) + (val % 256);
	++(occur[i]);
      }
    int i = 0;
    unsigned med = 0;
    while (med < sel.width () * sel.height () / 3)
      med += occur[i++];
    t1 = (i - 1) / 3;
    while (med < sel.width () * sel.height () * 2 / 3)
      med += occur[i++];
    t2 = (i - 1) / 3;
    c_->thresholds ()[1] = 240;
  }
  int c1 = c_->colors ()[0].rgb ();
  int c2 = c_->colors ()[1].rgb ();
  int c3 = c_->colors ()[2].rgb ();
  for (unsigned i = 0; i < sel.height (); ++i)
  {
    for (unsigned j = 0; j < sel.width (); ++j)
    {
      unsigned f = (((sel[i][j] >> 16) % 256) +
		    ((sel[i][j] >> 8) % 256) +
		    (sel[i][j] % 256)) / 3;
      if (f <= t1)
	sel[i][j] = c1;
      else if (f < t2)
	sel[i][j] = c2;
      else
	sel[i][j] = c3;
    }
    progress = i * 100 / sel.height ();
  }
}

QString Popart::name ()
{
  return "Warholian Portrait";
}

QString Popart::description ()
{
  return "Creates a PopArt portrait, Andy Warhol style.";
}
