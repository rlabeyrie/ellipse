#ifndef POPART_HH_
# define POPART_HH_

# include "plugin.hh"

# include "colorparameter.hh"

class Popart : public Plugin
{
public:
  Popart ();
  virtual void process (const Selection&, Selection&);
  virtual QString name ();
  virtual QString description ();
private:
  CheckParameter* auto_;
  ColorParameter* c_;
};

#endif /* !POPART_HH_ */
