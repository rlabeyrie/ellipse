#ifndef PLUGIN_HXX_
# define PLUGIN_HXX_

inline
void Plugin::histogram (Selection& sel)
{
  if (sel.width () < 256 || sel.height () < 100)
    return;
  unsigned histoa[256];
  unsigned histob[256];
  unsigned histoc[256];
  for (unsigned i = 0; i < 256; ++i)
  {
    histoa[i] = 0;
    histob[i] = 0;
    histoc[i] = 0;
  }
  unsigned maxi = 0;
  for (unsigned x = 0; x < sel.width (); ++x)
    for (unsigned y = 0; y < sel.height (); ++y)
    {
      unsigned val = sel[y][x];
      if (++histoa[(val >> 16) % 256] > maxi)
	++maxi;
      if (++histob[(val >> 8) % 256] > maxi)
	++maxi;
      if (++histoc[val % 256] > maxi)
	++maxi;
    }
  for (unsigned i = 0; i < 256; ++i)
    for (unsigned j = 0; j < 100; ++j)
    {
      unsigned col = sel[99 - j][i];
      unsigned a = ((col >> 16) % 256) / 2;
      unsigned b = ((col >> 8) % 256) / 2;
      unsigned c = (col % 256) / 2;
      col = a * 65536 + b * 256 + c;
      unsigned ref = j * maxi / 100;
      if (histoa[i] > ref)
	col += 8323072;
      if (histob[i] > ref)
	col += 32512;
      if (histoc[i] > ref)
	col += 127;
      sel[99 - j][i] = col;
    }
}

inline
void Plugin::addParameter (Parameter* param)
{
  params_.push_back (param);
}

inline
std::deque<Parameter*>& Plugin::params ()
{
  return params_;
}

#endif /* !PLUGIN_HXX_ */
