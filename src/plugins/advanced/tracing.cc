#include "tracing.hh"
#include <iostream>

ELLIPSE_PLUGIN(Tracing);

Tracing::Tracing ()
{
  grey_ = new CheckParameter ();
  grey_->setValue (true);
  grey_->setDescription ("Greyscale");
  contrast_ = new CheckParameter (grey_);
  contrast_->setValue (true);
  contrast_->setDescription ("Reinforce contrast");
  thres_ = new SliderParameter ();
  thres_->setMinimum (0);
  thres_->setMaximum (100);
  thres_->setStep (1);
  thres_->setValue (15);
  thres_->setDescription ("Threshold");
  addParameter (grey_);
  addParameter (contrast_);
  addParameter (thres_);
}

void Tracing::process (const Selection& img, Selection& res)
{
  int w = img.width ();
  int h = img.height ();
  unsigned mini = 255;
  double step = 100;
  step /= img.width ();
  if (grey_->value () && contrast_->value ())
    step /= 2;
  double coeff = 100;
  coeff /= 765 * thres_->value ();
  for (int x = 0; x < w; ++x)
  {
    for (int y = 0; y < h; ++y)
    {
      int max = 0;
      unsigned anc = img[y][x];
      unsigned anca = (anc >> 16) % 256;
      unsigned ancb = (anc >> 8) % 256;
      unsigned ancc = anc % 256;
      int ctr = anca + ancb + ancc;
      for (int i = -1; i <= 1; ++i)
	if (x + i >= 0 && x + i < w)
	  for (int j = -1; j <= 1; ++j)
	    if (y + j >= 0 && y + j < h)
	    {
	      unsigned tmp = img[y + j][x + i];
	      int val = ((tmp >> 16) % 256) + ((tmp >> 8) % 256) + (tmp % 256);
	      val -= ctr;
	      if (max < val)
		max = val;
	    }
      if (grey_->value ())
      {
	max = (765 - max) / 3;
	if (contrast_->value ())
	  max = max * max / 255;
	anca = max;
	ancb = max;
	ancc = max;
	anc = max * 65793;
      }
      double tmp = max * coeff;
      if (tmp >= 1)
	res[y][x] = anc;
      else
      {
	double cotmp = 255 * (1 - tmp);
	anca = anca * tmp + cotmp;
	ancb = ancb * tmp + cotmp;
	ancc = ancc * tmp + cotmp;
	res[y][x] = anca * 65536 + ancb * 256 + ancc;
      }
      if (anca < mini)
	mini = anca;
    }
    progress = x * step;
  }
  if (grey_->value () && mini)
  {
    double coeff = 255;
    coeff /= 255 - mini;
    for (int x = 0; x < w; ++x)
    {
      for (int y = 0; y < h; ++y)
      {
	unsigned anc = res[y][x];
	unsigned anca = coeff * (double)(((anc >> 16) % 256) - mini);
	unsigned ancb = coeff * (double)(((anc >> 8) % 256) - mini);
	unsigned ancc = coeff * (double)((anc % 256) - mini);
	res[y][x] = anca * 65536 + ancb * 256 + ancc;
      }
      progress = (w + x) * step;
    }
  }
}

QString Tracing::name ()
{
  return "Tracing Paper";
}

QString Tracing::description ()
{
  return "Converts to drawing effect.";
}
