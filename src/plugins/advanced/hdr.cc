#include "hdr.hh"
#include <iostream>
#include <QDir>

ELLIPSE_PLUGIN(Hdr);

Hdr::Hdr ()
{
  compress_ = new SliderParameter ();
  compress_->setMinimum (0);
  compress_->setMaximum (100);
  compress_->setStep (1);
  compress_->setValue (0);
  compress_->setDescription ("Linear compression from HDR to LDR");
  addParameter (compress_);
}

void Hdr::process (const Selection& img, Selection& res)
{
  QDir dir ("HDR");
  if (!dir.exists ())
    return;

  QStringList list = dir.entryList ();
  unsigned count = list.size ();
  QImage* imgs[count];
  unsigned long long expos[count];
  int w = 0;
  int h = 0;

  // Get pictures in a directory
  bool ext = false;
  unsigned i = 0;
  for (QStringList::iterator it = list.begin (); !ext && it != list.end ();
       ++it)
  {
    QImage* im = new QImage (dir.absolutePath () + "/" + *it);
    if (*it != "." && *it != "..")
    {
      if (!i)
      {
	w = im->width ();
	h = im->height ();
      }
      else
	if (w != im->width () || h != im->height ())
	  ext = true;
      imgs[i] = im;
      expos[i] = 0;
      ++i;
    }
  }
  if (ext || !w || !h || w != (int) (img.width ()) || h != (int) (img.height ()))
    return;
  count = i;

  // Compute exposure of each picture
  unsigned tmps[count];
  for (int j = 0; j < h; ++j)
    for (int i = 0; i < w; ++i)
    {
      bool ok = true;
      for (unsigned k = 0; ok && k < count; ++k)
      {
	unsigned tmp = imgs[k]->pixel (i, j);
	tmps[k] = ((tmp >> 16) % 256) + ((tmp >> 8) % 256) + (tmp % 256);
	if (tmps[k] == 765)
	  ok = false;
      }
      if (ok)
	for (unsigned k = 0; k < count; ++k)
	  expos[k] += tmps[k];
    }

  // Compute coeffs
  unsigned long long mini = expos[0];
  unsigned long long maxi = expos[0];
  for (unsigned i = 1; i < count; ++i)
  {
    if (expos[i] < mini)
      mini = expos[i];
    else
      if (expos[i] > maxi)
	maxi = expos[i];
  }
  if (!mini)
    return;
  double coeffs [count];
  for (unsigned k = 0; k < count; ++k)
  {
    coeffs[k] = maxi;
    coeffs[k] /= expos[k];
  }
  double spectrum = maxi * 255;
  spectrum /= mini;

  // Convert pictures to HDR and compute distribution curve
  unsigned step = 1 + spectrum / 4242;
  unsigned length = 1 + spectrum / step;
  double curve[length];
  for (unsigned i = 0; i < length; ++i)
    curve[i] = 0;

  for (int x = 0; x < w; ++x)
  {
    for (int y = 0; y < h; ++y)
    {
      double moy = 0;
      double fa = 0;
      double fb = 0;
      double fc = 0;
      for (unsigned i = 0; i < count; ++i)
      {
	unsigned col = (imgs[i]->pixel (x, y) % 16777216);
	double cola = (col >> 16) % 256;
	double colb = (col >> 8) % 256;
	double colc = col % 256;
	double relative = (cola + colb + colc) * coeffs[i];
	relative *= relative;
	moy += relative;
	fa += cola * coeffs[i] * relative;
	fb += colb * coeffs[i] * relative;
	fc += colc * coeffs[i] * relative;
      }
      fa /= moy;
      fb /= moy;
      fc /= moy;
      if (fb > fa)
      {
	if (fc > fb)
	  fa = fc;
	else
	  fa = fb;
      }
      else
	if (fc > fa)
	  fa = fc;
      unsigned index = fa / step;
      if (curve[index] < step)
	++curve[index];
    }
    progress = x * 50 / w;
  }

  // Compute spectrum deformation from the primitive of the distribution curve
  for (unsigned i = 1; i < length; ++i)
    curve[i] += curve[i - 1];
  if (curve[0] != curve[length - 1])
  {
    double offset = curve[0];
    double mul = 255;
    mul /= curve[length - 1] - offset;
    double compr = compress_->value ();
    double coeff = compr * 255 / (length - 1);
    for (unsigned i = 0; i < length; ++i)
      curve[i] = (coeff * i + (curve[i] - offset) * mul * (100 - compr)) / 100;
  }

  // Conversion to LDR
  for (int x = 0; x < w; ++x)
  {
    for (int y = 0; y < h; ++y)
    {
      double moy = 0;
      double fa = 0;
      double fb = 0;
      double fc = 0;

      for (unsigned i = 0; i < count; ++i)
      {
	unsigned col = (imgs[i]->pixel (x, y) % 16777216);
	double cola = (col >> 16) % 256;
	double colb = (col >> 8) % 256;
	double colc = col % 256;
	double relative = (cola + colb + colc) * coeffs[i];
	relative *= relative;
	moy += relative;
	fa += cola * coeffs[i] * relative;
	fb += colb * coeffs[i] * relative;
	fc += colc * coeffs[i] * relative;
      }
      fa /= moy;
      fb /= moy;
      fc /= moy;

      double ant = fa;
      if (fb > fa)
      {
      	if (fc > fb)
      	  ant = fc;
      	else
      	  ant = fb;
      }
      else
      	if (fc > fa)
      	  ant = fc;
      unsigned index = ant / step;
      unsigned ta = fa * curve[index] / ant;
      unsigned tb = fb * curve[index] / ant;
      unsigned tc = fc * curve[index] / ant;
      res[y][x] = ta * 65536 + tb * 256 + tc;
    }
    progress = 50 + x * 50 / w;
  }
}

QString Hdr::name ()
{
  return "HDR";
}

QString Hdr::description ()
{
  return "Generates a single HDR picture from several LDR ones.";
}
