#include "sky.hh"
#include <deque>
#include <iostream>

ELLIPSE_PLUGIN(Sky);

Sky::Sky ()
  : refa (255),
    refb (255),
    refc (255)
{
  contrast_ = new CheckParameter ();
  contrast_->setValue (true);
  contrast_->setText ("Keep contrast");
  dark_ = new CheckParameter (contrast_);
  dark_->setValue (true);
  dark_->setText ("Over white");
  tol_ = new SliderParameter ();
  tol_->setMinimum (1);
  tol_->setMaximum (100);
  tol_->setStep (1);
  tol_->setValue (25);
  tol_->setDescription ("Tolerance");
  radius_ = new SliderParameter ();
  radius_->setMinimum (1);
  radius_->setMaximum (10);
  radius_->setStep (1);
  radius_->setValue (3);
  radius_->setDescription ("Radius");
  start_ = new ColorParameter ();
  start_->addColor (QColor (255, 255, 255), 100);
  start_->setDescription ("Sky color");
  gradient_ = new GradientParameter ();
  gradient_->addColor (QColor (0x9bb5ff), 0);
  gradient_->addColor (QColor (0xffffff), 100);
  gradient_->setDescription ("Gradient of the new sky");
  addParameter (contrast_);
  addParameter (dark_);
  addParameter (tol_);
  addParameter (radius_);
  addParameter (start_);
  addParameter (gradient_);
}

inline
bool Sky::near (unsigned col)
{
  int a = refa - ((col >> 16) % 256);
  int b = refb - ((col >> 8) % 256);
  int c = refc - (col % 256);
  return (a * a + b * b + c * c) < ((tol_->value ()) * (tol_->value ()) * 3);
}

void Sky::process (const Selection& img, Selection& res)
{
  refa = start_->red ();
  refb = start_->green ();
  refc = start_->blue ();
  std::deque<unsigned> colors = gradient_->buildGradient (img.height ());

  int u = 0;
  unsigned x = img.width () / 2;
  bool f = false;
  while (!f && u < radius_->value ())
  {
    unsigned t = 0;
    while (!f && t < img.width () / 2)
    {
      int tmp = img[u][x + t];
      f = near(tmp);
      if (f)
	x = x + t;
      else
      {
	tmp = img[u][x - t];
	f = near(tmp);
	if (f)
	  x = x - t;
      }
      ++t;
    }
    ++u;
  }
  if (!f)
    return;
  unsigned y = 0;

  std::deque<int> lstx;
  std::deque<int> lsty;
  lstx.push_back (x);
  lsty.push_back (y);
  res[y][x] = colors[y];
  int sz = 0;
  int elt = 0;
  int maxi = 0;
  while (elt >= 0)
  {
    x = lstx[elt];
    y = lsty[elt];
    f = false;
    if (x > 0)
    {
      int tmp = res[y][x - 1];
      int col = colors[y];
      if (tmp != col)
      {
	f = near(tmp);
	if (f)
	{
	  res[y][x - 1] = col;
	  if (elt++ == sz)
	  {
	    ++sz;
	    lstx.push_back (x - 1);
	    lsty.push_back (y);
	  }
	  else
	  {
	    lstx[elt] = x - 1;
	    lsty[elt] = y;
	  }
	}
      }
    }

    if (!f && x + 1 < res.width ())
    {
      int tmp = res[y][x + 1];
      int col = colors[y];
      if (tmp != col)
      {
	f = near(tmp);
	if (f)
	{
	  res[y][x + 1] = col;
	  if (elt++ == sz)
	  {
	    ++sz;
	    lstx.push_back (x + 1);
	    lsty.push_back (y);
	  }
	  else
	  {
	    lstx[elt] = x + 1;
	    lsty[elt] = y;
	  }
	}
      }
    }

    if (!f && y > 0)
    {
      int tmp = res[y - 1][x];
      int col = colors[y - 1];
      if (tmp != col)
      {
	f = near(tmp);
	if (f)
	{
	  res[y - 1][x] = col;
	  if (elt++ == sz)
	  {
	    ++sz;
	    lstx.push_back (x);
	    lsty.push_back (y - 1);
	  }
	  else
	  {
	    lstx[elt] = x;
	    lsty[elt] = y - 1;
	  }
	}
      }
    }

    if (!f && y + 1 < res.height ())
    {
      int tmp = res[y + 1][x];
      int col = colors[y + 1];
      if (tmp != col)
      {
	f = near(tmp);
	if (f)
	{
	  res[y + 1][x] = col;
	  if (elt++ == sz)
	  {
	    ++sz;
	    lstx.push_back (x);
	    lsty.push_back (y + 1);
	  }
	  else
	  {
	    lstx[elt] = x;
	    lsty[elt] = y + 1;
	  }
	}
      }
    }

    if (!f)
    {
      for (int i = -radius_->value (); i <= radius_->value (); ++i)
	for (int j = -radius_->value (); j <= radius_->value (); ++j)
	  if (i * i + j * j > 1 && (int) x + i >= 0 && x + i < img.width ()
	      && (int) y + j >= 0 && y + j < img.height ())
	  {
	    int tmp = res[y + j][x + i];
	    int col = colors[y + j];
	    if (tmp != col)
	    {
	      f = near(tmp);
	      if (f)
	      {
		res[y + j][x + i] = col;
		if (elt++ == sz)
		{
		  ++sz;
		  lstx.push_back (x + i);
		  lsty.push_back (y + j);
		}
		else
		{
		  lstx[elt] = x + i;
		  lsty[elt] = y + j;
		}
	      }
	    }
	  }
    }

    if (!f)
      --elt;
    else
      if (elt > maxi)
	maxi = elt;
    progress = elt * 100 / maxi;
  }

  if (contrast_->value ())
  {
    int a;
    int b;
    int c;
    for (unsigned iy = 0; iy < img.height (); ++iy)
    {
      unsigned tcol = (colors[iy] % 16777216);
      float refa = (tcol >> 16) % 256;
      float refb = (tcol >> 8) % 256;
      float refc = tcol % 256;
      float reft = (refa + refb + refc) / 3;
      if (dark_->value ())
	reft = 255;
      for (unsigned ix = 0; ix < img.width (); ++ix)
      {
	if (tcol == res[iy][ix])
	{
	  unsigned col = (img[iy][ix] % 16777216);
	  float cola = (col >> 16) % 256;
	  float colb = (col >> 8) % 256;
	  float colc = col % 256;
	  float colt = (cola + colb + colc) / 3;
	  if (colt <= reft)
	  {
	    a = (int) (colt * refa / reft + .5);
	    b = (int) (colt * refb / reft + .5);
	    c = (int) (colt * refc / reft + .5);
	  }
	  else
	  {
	    a = 255 - (int) ((255 - colt) * (255 - refa) / (255 - reft) + .5);
	    b = 255 - (int) ((255 - colt) * (255 - refb) / (255 - reft) + .5);
	    c = 255 - (int) ((255 - colt) * (255 - refc) / (255 - reft) + .5);
	  }
	  res[iy][ix] = a * 65536 + b * 256 + c;
	}
      }
    }
  }
}

QString Sky::name ()
{
  return "Sky";
}

QString Sky::description ()
{
  return "Detects and recolors the sky in a picture.";
}
