#ifndef TRACING_HH_
# define TRACING_HH_

# include "plugin.hh"

# include "radioparameter.hh"
# include "checkparameter.hh"
# include "sliderparameter.hh"

class Tracing : public Plugin
{
public:
  Tracing ();
  virtual void process (const Selection&, Selection&);
  virtual QString name ();
  virtual QString description ();
private:
  CheckParameter* grey_;
  CheckParameter* contrast_;
  SliderParameter* thres_;
};

#endif /* !TRACING_HH_ */
