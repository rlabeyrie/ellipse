#ifndef HDR_HH_
# define HDR_HH_

# include "plugin.hh"

# include "sliderparameter.hh"

class Hdr : public Plugin
{
public:
  Hdr ();
  virtual void process (const Selection&, Selection&);
  virtual QString name ();
  virtual QString description ();
private:
  SliderParameter* compress_;
};

#endif /* !HDR_HH_ */
