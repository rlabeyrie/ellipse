#ifndef SKY_HH_
# define SKY_HH_

# include "plugin.hh"

# include "sliderparameter.hh"
# include "checkparameter.hh"
# include "gradientparameter.hh"

class Sky : public Plugin
{
public:
  Sky ();
  virtual void process (const Selection&, Selection&);
  virtual QString name ();
  virtual QString description ();
private:
  bool near (unsigned);
private:
  ColorParameter* start_;
  GradientParameter* gradient_;
  SliderParameter* radius_;
  SliderParameter* tol_;
  CheckParameter* contrast_;
  CheckParameter* dark_;
  int refa;
  int refb;
  int refc;
};

#endif /* !SKY_HH_ */
