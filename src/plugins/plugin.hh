#ifndef PLUGIN_HH_
# define PLUGIN_HH_

# include "selection.hh"
# include "parameter.hh"
# include <QString>
# include <deque>

# define ELLIPSE_PLUGIN(plugin_name)		\
    extern "C"					\
    {						\
      Plugin* maker##plugin_name ()		\
      {						\
	return new (plugin_name) ();		\
      }						\
      class Registrer				\
      {						\
	public:					\
	Registrer()				\
	{					\
	  g_maker = maker##plugin_name;		\
	}					\
      };					\
      Registrer r;				\
    }

class Plugin
{
public:
  virtual void process (const Selection&, Selection&) = 0;
  virtual QString name () = 0;
  virtual QString description () = 0;
  void histogram (Selection& sel);
  std::deque<Parameter*>& params ();
public:
  int progress;
protected:
  void addParameter (Parameter* param);
  std::deque<Parameter*> params_;
};

typedef Plugin* (*maker_plugin)();

extern maker_plugin g_maker;

# include "plugin.hxx"

#endif /* !PLUGIN_HH_ */
