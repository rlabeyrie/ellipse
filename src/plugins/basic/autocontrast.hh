#ifndef AUTOCONTRAST_HH_
# define AUTOCONTRAST_HH_

# include "plugin.hh"

class Autocontrast : public Plugin
{
public:
  Autocontrast ();
  virtual void process (const Selection&, Selection&);
  virtual QString name ();
  virtual QString description ();
};

#endif /* !AUTOCONTRAST_HH_ */
