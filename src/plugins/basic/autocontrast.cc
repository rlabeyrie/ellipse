#include "autocontrast.hh"

ELLIPSE_PLUGIN(Autocontrast);

Autocontrast::Autocontrast ()
{
}

void Autocontrast::process (const Selection&, Selection& sel)
{
  unsigned min = 255;
  unsigned max = 0;
  for (unsigned x = 0; x < sel.width (); ++x)
    for (unsigned y = 0; y < sel.height (); ++y)
    {
      unsigned val = sel[y][x];
      if ((val >> 16) % 256 < min)
	min = (val >> 16) % 256;
      if ((val >> 8) % 256 < min)
	min = (val >> 8) % 256;
      if (val % 256 < min)
	min = val % 256;
      if ((val >> 16) % 256 > max)
	max = (val >> 16) % 256;
      if ((val >> 8) % 256 > max)
	max = (val >> 8) % 256;
      if (val % 256 > max)
	max = val % 256;
    }
  if (max == min)
    return;
  double coeff = 255;
  coeff /= max - min;
  for (unsigned x = 0; x < sel.width (); x++)
  {
    for (unsigned y = 0; y < sel.height (); y++)
    {
      unsigned tmp = sel[y][x];
      int a = (tmp >> 16) % 256;
      int b = (tmp >> 8) % 256;
      int c = tmp % 256;
      a = (a - min) * coeff + .5;
      b = (b - min) * coeff + .5;
      c = (c - min) * coeff + .5;
      sel[y][x] = a * 65536 + b * 256 + c;
    }
    progress = x * 100 / sel.width ();
  }
}

QString Autocontrast::name ()
{
  return "Auto contrast";
}

QString Autocontrast::description ()
{
  return "Automatic adjustment of contrast.";
}
