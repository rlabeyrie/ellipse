#include "luminance.hh"

ELLIPSE_PLUGIN(Luminance);

Luminance::Luminance ()
{
  light_ = new SliderParameter ();
  light_->setMinimum (0);
  light_->setMaximum (100);
  light_->setStep (1);
  light_->setValue (0);
  light_->setDescription ("Reinforce lightness");
  dark_ = new SliderParameter ();
  dark_->setMinimum (0);
  dark_->setMaximum (100);
  dark_->setStep (1);
  dark_->setValue (0);
  dark_->setDescription ("Reinforce darkness");
  add_ = new SliderParameter ();
  add_->setMinimum (-255);
  add_->setMaximum (255);
  add_->setStep (5);
  add_->setValue (0);
  add_->setDescription ("Offset");
  addParameter (light_);
  addParameter (dark_);
  addParameter (add_);
}

void Luminance::process (const Selection&, Selection& sel)
{
  int l = 100 - light_->value ();
  int d = 100 - dark_->value ();
  int add = add_->value ();
  if (l == 100 && d == 100 && add == 0)
    return;
  for (unsigned y = 0; y < sel.height (); ++y)
  {
    for (unsigned x = 0; x < sel.width (); ++x)
    {
      unsigned tmp = sel[y][x];
      int a = (tmp >> 16) % 256;
      int b = (tmp >> 8) % 256;
      int c = tmp % 256;
      a = (a * d + 25500 - (255 - a) * l) / 200 + add;
      if (a < 0)
	a = 0;
      if (a > 255)
	a = 255;
      b = (b * d + 25500 - (255 - b) * l) / 200 + add;
      if (b < 0)
	b = 0;
      if (b > 255)
	b = 255;
      c = (c * d + 25500 - (255 - c) * l) / 200 + add;
      if (c < 0)
	c = 0;
      if (c > 255)
	c = 255;
      sel[y][x] = a * 65536 + b * 256 + c;
    }
    progress = y * 100 / sel.height ();
  }
}

QString Luminance::name ()
{
  return "Luminance adjustment";
}

QString Luminance::description ()
{
  return "Modifies lightness or darkness of the picture.";
}
