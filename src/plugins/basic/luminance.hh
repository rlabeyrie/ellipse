#ifndef LUMINANCE_HH_
# define LUMINANCE_HH_

# include "plugin.hh"

# include "sliderparameter.hh"

class Luminance : public Plugin
{
public:
  Luminance ();
  virtual void process (const Selection&, Selection&);
  virtual QString name ();
  virtual QString description ();
private:
  SliderParameter* light_;
  SliderParameter* dark_;
  SliderParameter* add_;
};

#endif /* !LUMINANCE_HH_ */
